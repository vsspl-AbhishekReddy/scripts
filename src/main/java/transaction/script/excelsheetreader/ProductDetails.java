/*package transaction.script.excelsheetreader;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import transaction.script.constants.PlatformConstants;
import transaction.script.models.LocaleNameTo;
import transaction.script.models.ProductTo;
import transaction.script.models.UnitDeatilsTo;
import transaction.script.platform.utils.HsqlDBUtil;
import transaction.script.platform.utils.SearchProductTo;

import com.google.gson.Gson;

public class ProductDetails {
	static Connection connection = HsqlDBUtil.dbConnection;
	static Gson gson = null;
	static {
		gson = new Gson();
	}

	public static ArrayList<ProductTo> getProductDetails(String productIds, String countryId) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ArrayList<ProductTo> productToLst = new ArrayList<ProductTo>();
		productIds=productIds.replaceAll(",", ";");
		System.out.println(productIds);
		String[] listofIds = productIds.split(";");
		Map<String, ProductTo> map = new HashMap<String, ProductTo>();
		try {
			String query = createproductInQuery(null, productIds);
			preparedStatement = connection.prepareStatement(query);
			// preparedStatement.setString(1, "335");
			preparedStatement.setString(1, countryId);
			for (int i = 1; i <= listofIds.length; i++) {
				preparedStatement.setString(i + 1, listofIds[i - 1]);
			}
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				ProductTo product = map.get(resultSet.getString("vsspsc_code"));
				if (product != null) {
					UnitDeatilsTo details = new UnitDeatilsTo();
					details.setUnitId(resultSet.getInt("unit_id"));
					details.setUnitName(resultSet.getString("unit_name"));
					details.setConversion(resultSet.getDouble("conversion"));
					product.getListofUnits().add(details);
				} else {
					System.out.println("else");
					ProductTo objProductTo = new ProductTo();
					objProductTo.setProductId(resultSet.getString("vsspsc_code"));
					objProductTo.setProductName(resultSet.getString("display_name"));
					objProductTo.setBaseUnitId(resultSet.getInt("unit_id") + "");
					objProductTo.setBaseUnitName(resultSet.getString("unit_name"));
					objProductTo.setCommodityId(resultSet.getString("commodity_id"));
					objProductTo.setCommodityName(resultSet.getString("commodity"));
					objProductTo.setVarietyId(resultSet.getString("variety_id"));
					objProductTo.setVarietyName(resultSet.getString("variety"));
					objProductTo.setScientific_name(resultSet.getString("scientific_name"));
					objProductTo.setUniversal_name(resultSet.getString("universal_name"));
					objProductTo.setNotes(resultSet.getString("notes"));
					objProductTo.setWeblinks(resultSet.getString("weblinks"));
					objProductTo.setAlsoKnownAs(resultSet.getString("also_known_as"));
					objProductTo.setImageUrl(resultSet.getString("image"));
					objProductTo.setQualifiers(resultSet.getString("qualifiers"));
					objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
					objProductTo.setGroupId(resultSet.getString("group_id"));
					objProductTo.setGroupName(resultSet.getString("group_name"));
					objProductTo.setCrops(resultSet.getString("crops"));
					objProductTo.setCompany(resultSet.getString("company_id"));
					objProductTo.setIntegredients(resultSet.getString("ingredients"));
					UnitDeatilsTo details = new UnitDeatilsTo();
					details.setUnitId(resultSet.getInt("unit_id"));
					details.setUnitName(resultSet.getString("unit_name"));
					details.setConversion(resultSet.getDouble("conversion"));
					objProductTo.getListofUnits().add(details);
					objProductTo.setCustomunits(getProductCustomUnits(objProductTo.getProductId(), countryId));
					productToLst.add(objProductTo);
					map.put(resultSet.getString("vsspsc_code"), objProductTo);
				}
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		System.out.println(productToLst.size());
		System.out.println(" product list::::::::::"+gson.toJson(productToLst));
		return productToLst;
	}

	public static Set<ProductTo> ProductsSearch(String subStrings, String countryId, String stateId, String groupsIds) {
		Set<ProductTo> productsset = new HashSet<ProductTo>();
		String spellcheckWord = subStrings;
		subStrings = subStrings.replaceAll("-", " ");
		countryId = countryId.replace(",", " ");
		String URL = "";
		try {
			subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
			spellcheckWord = java.net.URLEncoder.encode(spellcheckWord, "UTF-8");
			subStrings = subStrings.replaceAll("\\+", "%20AND%20");
			countryId = countryId.replace(" ", "%20OR%20");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		int rows = 5000;
		if (countryId.equalsIgnoreCase("ALL")) {
			countryId = "*";
			rows = 5000;
		}
		// subStrings = subStrings.replaceAll(" ", "%20AND%20");
		Response clientResponse = null;
		if (groupsIds.equals("0")) {
			URL = PlatformConstants.COMMONDATA_SEARCH + "(countryId:(" + countryId + ")%20AND%20*" + subStrings + "*)&wt=json&spellcheck.dictionary=test&spellcheck=true&" + "qf=%20alsoKnownAs%5E4"
					+ "%20productName%5E5" + "%20localeNames.displayName%5E3" + "%20company1%20crops%5E2%20ingredients1&start=0&rows=" + rows + "&fl=_json_&spellcheck.q="
					+ spellcheckWord.replaceAll(" ", "%20OR%20");
		} else {
			String contructedGroup = "";
			if (groupsIds.contains(",")) {
				String[] groups = groupsIds.split(",");
				for (int i = 0; i < groups.length; i++) {
					contructedGroup = contructedGroup + " " + groups[i];
				}
			} else {
				contructedGroup = groupsIds;
			}
			contructedGroup = contructedGroup.trim();
			contructedGroup = contructedGroup.replaceAll(" ", "%20");

			URL = PlatformConstants.COMMONDATA_SEARCH + "(countryId:(" + countryId + ")%20AND%20groupId:(" + contructedGroup + ")%20AND%20*" + subStrings
					+ "*)&wt=json&spellcheck.dictionary=test&spellcheck=true&" + "qf=%20alsoKnownAs%5E4" + "%20productName%5E5" + "%20localeNames.displayName%5E3"
					+ "%20company%5E1%20crops%5E2%20ingredients%5E1&start=0&rows=" + rows + "&fl=_json_&spellcheck.q=" + spellcheckWord.replaceAll(" ", "%20OR%20");
		}
		System.out.println(URL);
		WebTarget webTarget = HsqlDBUtil.client().target(URL);
		Invocation.Builder invocationBuilder = webTarget.request();
		clientResponse = invocationBuilder.get();
		String output = clientResponse.readEntity(String.class);
		JSONObject objJson = new JSONObject(output);
		JSONObject responsedata = objJson.getJSONObject("response");
		String result = responsedata.toString();
		JSONObject obj = new JSONObject(result);
		String suggestedQuery = null;
		if (obj.get("numFound").toString().equals("0")) {
			JSONObject temp = objJson.getJSONObject("spellcheck");
			JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
			if (suggestionsArray.length() == 0)
				return productsset;
			JSONArray misSpellArray = new JSONArray(suggestionsArray.get(3).toString());
			JSONArray queryAndSuggestion = new JSONArray(misSpellArray.get(5).toString());
			suggestedQuery = subStrings = queryAndSuggestion.get(1).toString();
			// System.out.println("Did you mean..."+suggestedQuery);
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = subStrings.replaceAll(" ", "%20AND%20");
			URL = PlatformConstants.COMMONDATA_SEARCH
					+ "(countryId:("
					+ countryId
					+ ")%20AND%20*"
					+ subStrings
					+ "*)&wt=json&spellcheck.dictionary=test&spellcheck=true&spellcheck.build=true&qf=productName%20alsoKnownAs%20localeNames.displayName%20company%20crops%20ingredients&start=0&rows=1500&fl=_json_";
			webTarget = HsqlDBUtil.client().target(URL);
			invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			output = clientResponse.readEntity(String.class);
			objJson = new JSONObject(output);
			responsedata = objJson.getJSONObject("response");
			result = responsedata.toString();
			obj = new JSONObject(result);
		}
		Object test = obj.get("docs");
		JSONArray array = new JSONArray(test.toString());
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj1 = new JSONObject(array.get(i).toString());
			SearchProductTo objSearchProductTo = gson.fromJson(obj1.get("_json_").toString(), SearchProductTo.class);
			if (objSearchProductTo.getVarietyId().subSequence(9, 12).equals("000")) {
				ProductTo productTo = makeProductsTo(objSearchProductTo, stateId, "SIGNUP");
				productsset.add(productTo);
			}
		}
		return productsset;
	}

	private static ProductTo makeProductsTo(SearchProductTo objSearchProductTo, String stateId, String type) {
		ProductTo objProductTo = new ProductTo();
		objProductTo.setProductId(objSearchProductTo.getProductId());
		objProductTo.setProductName(objSearchProductTo.getProductName());
		objProductTo.setBaseUnitId(objSearchProductTo.getBaseUnitId());
		objProductTo.setBaseUnitName(objSearchProductTo.getBaseUnitName());
		objProductTo.setCommodityId(objSearchProductTo.getCommodityId());
		objProductTo.setCommodityName(objSearchProductTo.getCommodityName());
		objProductTo.setCrops(objSearchProductTo.getCrops());
		if (objSearchProductTo.getAlsoKnownAs() != null && !objSearchProductTo.getAlsoKnownAs().trim().isEmpty()) {
			objProductTo.setAlsoKnownAs(objSearchProductTo.getAlsoKnownAs());
		} else {
			objProductTo.setAlsoKnownAs("");
		}
		objProductTo.setGroupId(objSearchProductTo.getGroupId());
		objProductTo.setGroupName(objSearchProductTo.getGroupName());
		objProductTo.setVarietyId(objSearchProductTo.getVarietyId());
		objProductTo.setVarietyName(objSearchProductTo.getVarietyName());
		if (objSearchProductTo.getImageUrl() != null) {
			objProductTo.setImageUrl(HsqlDBUtil.cloudFrontCommonDataImageUrl(objSearchProductTo.getImageUrl()));
		}
		if (!("SIGNUP".equalsIgnoreCase(type))) {
			objProductTo.setQualifiers(objSearchProductTo.getQualifiers());
			if (objProductTo.getQualifiers() == null || objProductTo.getQualifiers().isEmpty()) {
				objProductTo.setQualifiers(getBaseProductQualifiers(objProductTo.getProductId(), objProductTo.getCommodityId()));
			}
			objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
		}

		objProductTo.setProductDescription(objSearchProductTo.getProductDescription());
		objProductTo.setLocaleNames(objSearchProductTo.getStateLocalName().get(stateId));
		if (null == objSearchProductTo.getStateLocalName().get(stateId)) {
			List<LocaleNameTo> lstOfLocaleNames = getProductLocaleNames(stateId, objSearchProductTo.getCommodityId());
			objProductTo.setLocaleNames(lstOfLocaleNames);
		}
		objProductTo.setProductDisplayName(getProductDisplayName(objProductTo.getProductName(), objProductTo.getLocaleNames()));
		return objProductTo;
	}

	private static String getProductDisplayName(String productName, List<LocaleNameTo> lstOfLocaleNames) {
		String productDisplayName = productName;
		if (2 == lstOfLocaleNames.size()) {
			for (LocaleNameTo objLocaleName : lstOfLocaleNames) {
				// Concatenating other than hindi language as local name for
				// india users as we are considering hindi as local lang for all
				// indian users.
				if (2 != objLocaleName.getLanguageId()) {
					if (!productName.trim().equalsIgnoreCase(objLocaleName.getDisplayName().trim())) {
						productDisplayName = productDisplayName + " (" + objLocaleName.getDisplayName() + ")";
					}
				}
			}
		} else if (1 == lstOfLocaleNames.size()) {
			for (LocaleNameTo objLocaleName : lstOfLocaleNames) {
				// Excluding local name concatenation if the local name is in
				// english.
				if (1 != objLocaleName.getLanguageId()) {
					if (!productName.trim().equalsIgnoreCase(objLocaleName.getDisplayName().trim())) {
						productDisplayName = productDisplayName + " (" + objLocaleName.getDisplayName() + ")";
					}
				}
			}
		}
		return productDisplayName;
	}

	public static String getBaseProductQualifiers(String vsspscCode, String commodityId) {
		String qualifiers = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String query = "SELECT qualifiers FROM PRODUCTS WHERE vsspsc_code = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?)";
		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, commodityId + "000");
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next()) {
				qualifiers = resultSet.getString("qualifiers");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return qualifiers;
	}

	private static String createproductInQuery(String sessionId, String productIds) {
		String query = " SELECT P.ingredients, P.crops, P.company_id,P.vsspsc_code, P.image,P.qualifiers, PL.display_name,P.commodity_id,P.commodity, P.variety_id,P.variety,PG.group_id,P.ALSO_KNOWN_AS,P.SCIENTIFIC_NAME,P.WEBLINKS,P.NOTES,"
				+ " P.UNIVERSAL_NAME,P.HSCODE, PG.group_name, U.unit_id, U.unit_name,U.conversion FROM Products P INNER JOIN Units U on P.unit_id = U.base_unit_id INNER JOIN PRODUCT_GROUPS PG on "
				+ " P.group_id = PG.group_id INNER JOIN PRODUCT_TO_COUNTRY PC on P.vsspsc_code = PC.vsspsc_code INNER JOIN PRODUCTS_LOCALE_NAMES PL on PL.vsspsc_code = PC.vsspsc_code WHERE PL.language_id =1 "
				+ " AND PC.country_id = PL.country_id AND PC.country_id = ? AND P.UNIVERSAL_NAME IN (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < productIds.split(";").length; i++) {
			queryBuilder.append(" ?");
			if (i != productIds.split(";").length - 1) {
				queryBuilder.append(",");
			}
		}
		queryBuilder.append(") ORDER BY PL.display_name ASC");
		System.out.println(queryBuilder.toString());
		return queryBuilder.toString();
	}

	public static String qualifiersort(String qualifier) {
		StringBuilder strbui = new StringBuilder();
		if ((qualifier != null) && !(qualifier.equals(""))) {
			String[] array = qualifier.split(",");
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				strbui.append(array[i]);
				if ((i + 1) < array.length) {
					strbui.append(",");
				}
			}
			qualifier = strbui.toString();

		}
		return qualifier;
	}

	private static List<UnitDeatilsTo> getProductCustomUnits(String vsspscCode, String countryId) {
		PreparedStatement preparedStatement = null;
		List<UnitDeatilsTo> customUnitsList = new ArrayList<>();
		ResultSet resultSet = null;
		String query = "SELECT custom_unitid, custom_unitname, conversion FROM PRODUCTS_TO_CUSTOMUNITS PC" + " INNER JOIN CUSTOMUNITS C on PC.custom_unitid = C.custom_unitid"
				+ " WHERE PC.vsspsc_code = ? AND C.country_id = ?";
		try {
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, vsspscCode);
			preparedStatement.setString(2, countryId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				UnitDeatilsTo objUnitDeatilsTo = new UnitDeatilsTo();
				objUnitDeatilsTo.setUnitId(resultSet.getInt("custom_unitid"));
				objUnitDeatilsTo.setUnitName(resultSet.getString("custom_unitname"));
				objUnitDeatilsTo.setConversion(resultSet.getDouble("conversion"));
				customUnitsList.add(objUnitDeatilsTo);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Collections.sort(customUnitsList, new Comparator<UnitDeatilsTo>() {
			@Override
			public int compare(UnitDeatilsTo o1, UnitDeatilsTo o2) {
				String placeName1 = o1.getUnitName();
				String placeName2 = o2.getUnitName();
				// ascending order
				return placeName1.compareTo(placeName2);
			}
		});
		return customUnitsList;
	}
	
	private static List<LocaleNameTo> getProductLocaleNames(String stateId, String commodityId) {
		List<LocaleNameTo> lstOfLocaleNameTo = new ArrayList<LocaleNameTo>();
		try {
			String query = "";
			if (stateId.substring(0, 3).contains("335")) {
				query = " SELECT PLN.DISPLAY_NAME, LAN.LANGUAGE_NAME, LAN.LANGUAGE_ID FROM LANGUAGES LAN INNER JOIN PRODUCTS_LOCALE_NAMES PLN ON LAN.LANGUAGE_ID = PLN.LANGUAGE_ID INNER JOIN PRODUCTS PDT ON "
						+ " PLN.VSSPSC_CODE = PDT.VSSPSC_CODE WHERE PDT.VSSPSC_CODE  = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?) AND LANGUAGE_ID IN(2,(SELECT DEFAULT_LANGUAGE_ID FROM STATES WHERE STATE_ID = ?))";
			} else {
				query = " SELECT PLN.DISPLAY_NAME, LAN.LANGUAGE_NAME, LAN.LANGUAGE_ID FROM LANGUAGES LAN INNER JOIN PRODUCTS_LOCALE_NAMES PLN ON LAN.LANGUAGE_ID = PLN.LANGUAGE_ID INNER JOIN PRODUCTS PDT ON PLN.VSSPSC_CODE = "
						+ " PDT.VSSPSC_CODE WHERE PDT.VSSPSC_CODE  = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?) AND LANGUAGE_ID = (SELECT DEFAULT_LANGUAGE_ID FROM STATES WHERE STATE_ID = ?))";
			}
			System.out.println(query);
			System.out.println(stateId);
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, commodityId + "000");
			//preparedStatement.setString(2, productId);
			preparedStatement.setString(2, stateId);
			ResultSet objResultSet = preparedStatement.executeQuery();
			while (objResultSet.next()) {
				LocaleNameTo objLocaleNames = new LocaleNameTo();
				String localeName = objResultSet.getString("DISPLAY_NAME");
				if (null != localeName && !localeName.trim().isEmpty()) {
					objLocaleNames.setDisplayName(localeName);
					objLocaleNames.setLanguageId(objResultSet.getInt("LANGUAGE_ID"));
					objLocaleNames.setLanguageName(objResultSet.getString("LANGUAGE_NAME"));
					lstOfLocaleNameTo.add(objLocaleNames);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstOfLocaleNameTo;
	}
}
*/