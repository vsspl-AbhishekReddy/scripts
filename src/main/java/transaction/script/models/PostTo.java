package transaction.script.models;

public class PostTo {
	private transient String mobileNum;
	private String subject;
	private String message;
	private String quantity;
	private String threadCategory;
	private String txtFilesattachments;
	private String attachments;
	private boolean isExpressInterest;
	private String audioURL;
	private String currencyCode;
	private boolean isprivate;
	private String productid;
	private String productName;
	private String unitsid;
	private String qualifiers;
	private String offerPriceUnitsid;
	private String offerprice;
	private String postCreatedDate;
	private String postExpiryDate;
	private String status;
	private String identifierTimestamp;
	private String responseInfo;
	private String receivers;

	public String getReceivers() {
		return receivers;
	}

	public void setReceivers(String receivers) {
		this.receivers = receivers;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getThreadCategory() {
		return threadCategory;
	}

	public void setThreadCategory(String threadCategory) {
		this.threadCategory = threadCategory;
	}

	public String getTxtFilesattachments() {
		return txtFilesattachments;
	}

	public void setTxtFilesattachments(String txtFilesattachments) {
		this.txtFilesattachments = txtFilesattachments;
	}

	public String getAttachments() {
		return attachments;
	}

	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}

	public boolean isExpressInterest() {
		return isExpressInterest;
	}

	public void setExpressInterest(boolean isExpressInterest) {
		this.isExpressInterest = isExpressInterest;
	}

	public String getAudioURL() {
		return audioURL;
	}

	public void setAudioURL(String audioURL) {
		this.audioURL = audioURL;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public boolean isIsprivate() {
		return isprivate;
	}

	public void setIsprivate(boolean isprivate) {
		this.isprivate = isprivate;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitsid() {
		return unitsid;
	}

	public void setUnitsid(String unitsid) {
		this.unitsid = unitsid;
	}

	public String getQualifiers() {
		return qualifiers;
	}

	public void setQualifiers(String qualifiers) {
		this.qualifiers = qualifiers;
	}

	public String getOfferPriceUnitsid() {
		return offerPriceUnitsid;
	}

	public void setOfferPriceUnitsid(String offerPriceUnitsid) {
		this.offerPriceUnitsid = offerPriceUnitsid;
	}

	public String getOfferprice() {
		return offerprice;
	}

	public void setOfferprice(String offerprice) {
		this.offerprice = offerprice;
	}

	public String getPostCreatedDate() {
		return postCreatedDate;
	}

	public void setPostCreatedDate(String postCreatedDate) {
		this.postCreatedDate = postCreatedDate;
	}

	public String getPostExpiryDate() {
		return postExpiryDate;
	}

	public void setPostExpiryDate(String postExpiryDate) {
		this.postExpiryDate = postExpiryDate;
	}

	public String getMobileNum() {
		return mobileNum;
	}

	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIdentifierTimestamp() {

		return identifierTimestamp;
	}

	public void setIdentifierTimestamp(String identifierTimestamp) {
		this.identifierTimestamp = identifierTimestamp;
	}

	public String getResponseInfo() {
		return responseInfo;
	}

	public void setResponseInfo(String responseInfo) {
		this.responseInfo = responseInfo;
	}

}