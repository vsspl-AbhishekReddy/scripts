package transaction.script.models;

public class UserEssentialsTo {
	private String oldName;
	private String oldBusinessName;
	private String oldMobileNumber;
	private String newName;
	private String newBusinessName;
	private String newMobileNumber;
	private String status;
	private String preCreatedRegInfo;
	
	public String getOldName() {
		return oldName;
	}
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	public String getOldBusinessName() {
		return oldBusinessName;
	}
	public void setOldBusinessName(String oldBusinessName) {
		this.oldBusinessName = oldBusinessName;
	}
	public String getOldMobileNumber() {
		return oldMobileNumber;
	}
	public void setOldMobileNumber(String oldMobileNumber) {
		this.oldMobileNumber = oldMobileNumber;
	}
	public String getNewName() {
		return newName;
	}
	public void setNewName(String newName) {
		this.newName = newName;
	}
	public String getNewBusinessName() {
		return newBusinessName;
	}
	public void setNewBusinessName(String newBusinessName) {
		this.newBusinessName = newBusinessName;
	}
	public String getNewMobileNumber() {
		return newMobileNumber;
	}
	public void setNewMobileNumber(String newMobileNumber) {
		this.newMobileNumber = newMobileNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPreCreatedRegInfo() {
		return preCreatedRegInfo;
	}
	public void setPreCreatedRegInfo(String preCreatedRegInfo) {
		this.preCreatedRegInfo = preCreatedRegInfo;
	}
	
}
