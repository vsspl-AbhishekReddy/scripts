package transaction.script.models;

import java.util.List;

public class RegistrationTo {
	private String name;
	private String mobileNumber;
	private PlaceTo apmcTo;
	private String businessName;
	private String businessTypeId;
	private String businessTypeName;
	private List<ProductTo> lstOfProducts;
	private PlaceTo locationTo;
	private List<PlaceTo> lstBizInterest;
	private String businessKey;
	private String bizUUID;
	private String apmcLicenseNo;
	private boolean isPreCreated;
	private String password;
	private String mobileTelecomCode;
	private int otpflag;
	private String licenseNo;
	private String locationId;
	private String productIds;
	private boolean isPreCreatedRegComplete;
	private String preCreatedRegInfo;
	private String licenseValidityDate;
	private String status;
	private String duplicateOfMobileNumber;
	private String email;
	private String website;
	private String address;
	private String otherPhoneNumbers;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber
	 *            the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the apmcTo
	 */
	public PlaceTo getApmcTo() {
		return apmcTo;
	}

	/**
	 * @param apmcTo
	 *            the apmcTo to set
	 */
	public void setApmcTo(PlaceTo apmcTo) {
		this.apmcTo = apmcTo;
	}

	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return the businessTypeId
	 */
	public String getBusinessTypeId() {
		return businessTypeId;
	}

	/**
	 * @param businessTypeId
	 *            the businessTypeId to set
	 */
	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	/**
	 * @return the businessTypeName
	 */
	public String getBusinessTypeName() {
		return businessTypeName;
	}

	/**
	 * @param businessTypeName
	 *            the businessTypeName to set
	 */
	public void setBusinessTypeName(String businessTypeName) {
		this.businessTypeName = businessTypeName;
	}

	/**
	 * @return the lstOfProducts
	 */
	public List<ProductTo> getLstOfProducts() {
		return lstOfProducts;
	}

	/**
	 * @param lstOfProducts
	 *            the lstOfProducts to set
	 */
	public void setLstOfProducts(List<ProductTo> lstOfProducts) {
		this.lstOfProducts = lstOfProducts;
	}

	/**
	 * @return the locationTo
	 */
	public PlaceTo getLocationTo() {
		return locationTo;
	}

	/**
	 * @param locationTo
	 *            the locationTo to set
	 */
	public void setLocationTo(PlaceTo locationTo) {
		this.locationTo = locationTo;
	}

	/**
	 * @return the lstBizInterest
	 */
	public List<PlaceTo> getLstBizInterest() {
		return lstBizInterest;
	}

	/**
	 * @param lstBizInterest
	 *            the lstBizInterest to set
	 */
	public void setLstBizInterest(List<PlaceTo> lstBizInterest) {
		this.lstBizInterest = lstBizInterest;
	}

	/**
	 * @return the businessKey
	 */
	public String getBusinessKey() {
		return businessKey;
	}

	/**
	 * @param businessKey
	 *            the businessKey to set
	 */
	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	/**
	 * @return the bizUUID
	 */
	public String getBizUUID() {
		return bizUUID;
	}

	/**
	 * @param bizUUID
	 *            the bizUUID to set
	 */
	public void setBizUUID(String bizUUID) {
		this.bizUUID = bizUUID;
	}

	/**
	 * @return the apmcLicenseNo
	 */
	public String getApmcLicenseNo() {
		return apmcLicenseNo;
	}

	/**
	 * @param apmcLicenseNo
	 *            the apmcLicenseNo to set
	 */
	public void setApmcLicenseNo(String apmcLicenseNo) {
		this.apmcLicenseNo = apmcLicenseNo;
	}

	/**
	 * @return the isPreCreated
	 */
	public boolean getIsPreCreated() {
		return isPreCreated;
	}

	/**
	 * @param isPreCreated
	 *            the isPreCreated to set
	 */
	public void setIsPreCreated(boolean isPreCreated) {
		this.isPreCreated = isPreCreated;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the mobileTelecomCode
	 */
	public String getMobileTelecomCode() {
		return mobileTelecomCode;
	}

	/**
	 * @param mobileTelecomCode
	 *            the mobileTelecomCode to set
	 */
	public void setMobileTelecomCode(String mobileTelecomCode) {
		this.mobileTelecomCode = mobileTelecomCode;
	}

	/**
	 * @return the otpflag
	 */
	public int getOtpflag() {
		return otpflag;
	}

	/**
	 * @param otpflag
	 *            the otpflag to set
	 */
	public void setOtpflag(int otpflag) {
		this.otpflag = otpflag;
	}

	/**
	 * @return the licenseNo
	 */
	public String getLicenseNo() {
		return licenseNo;
	}

	/**
	 * @param licenseNo
	 *            the licenseNo to set
	 */
	public void setLicenseNo(String licenseNo) {
		this.licenseNo = licenseNo;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the productIds
	 */
	public String getProductIds() {
		return productIds;
	}

	/**
	 * @param productIds
	 *            the productIds to set
	 */
	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}

	/**
	 * @return the preCreatedRegInfo
	 */
	public String getPreCreatedRegInfo() {
		return preCreatedRegInfo;
	}

	/**
	 * @param preCreatedRegInfo
	 *            the preCreatedRegInfo to set
	 */
	public void setPreCreatedRegInfo(String preCreatedRegInfo) {
		this.preCreatedRegInfo = preCreatedRegInfo;
	}

	/**
	 * @return the isPreCreatedRegComplete
	 */
	public boolean getIsPreCreatedRegComplete() {
		return isPreCreatedRegComplete;
	}

	/**
	 * @param isPreCreatedRegComplete
	 *            the isPreCreatedRegComplete to set
	 */
	public void setIsPreCreatedRegComplete(boolean isPreCreatedRegComplete) {
		this.isPreCreatedRegComplete = isPreCreatedRegComplete;
	}

	/**
	 * @return the licenseValidityDate
	 */
	public String getLicenseValidityDate() {
		return licenseValidityDate;
	}

	/**
	 * @param licenseValidityDate
	 *            the licenseValidityDate to set
	 */
	public void setLicenseValidityDate(String licenseValidityDate) {
		this.licenseValidityDate = licenseValidityDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDuplicateOfMobileNumber() {
		return duplicateOfMobileNumber;
	}

	public void setDuplicateOfMobileNumber(String duplicateOfMobileNumber) {
		this.duplicateOfMobileNumber = duplicateOfMobileNumber;
	}

	/*
		public Object get_id() {
			return _id;
		}

		public void set_id(Object _id) {
			this._id = _id;
		}*/

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOtherPhoneNumbers() {
		return otherPhoneNumbers;
	}

	public void setOtherPhoneNumbers(String otherPhoneNumbers) {
		this.otherPhoneNumbers = otherPhoneNumbers;
	}
}