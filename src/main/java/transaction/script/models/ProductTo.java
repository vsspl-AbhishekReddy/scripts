/*******************************************************************************
 * Copyright (c) 2013 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *  
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 *******************************************************************************/
package transaction.script.models;

import java.util.ArrayList;
import java.util.List;

public class ProductTo implements Comparable<ProductTo> {

	private String productId;
	private String productName;
	private String baseUnitId;
	private String baseUnitName;
	private String commodityId;
	private String commodityName;
	private String varietyId;
	private String varietyName;
	private String groupId;
	private String groupName;
	private String imageUrl;
	private String alsoKnownAs;
	private String productDescription;
	private String qualifiers;
	private List<LocaleNameTo> localeNames;
	private List<UnitDeatilsTo> listofUnits = new ArrayList<UnitDeatilsTo>();
	private List<UnitDeatilsTo> customunits;
	private String universal_name;
	private String localeName;
	private String scientific_name;
	private String weblinks;
	private String notes;
	private String productDisplayName;
	private String integredients;
	private String company;
	private String crops;
	private String parentGroupId;
	private String parentGroupName;
	private String productType;
	private String parentIds;
	private String ccpQualifier;
	private String fullProfileUrl;

	public String getFullProfileUrl() {
		return fullProfileUrl;
	}

	public void setFullProfileUrl(String fullProfileUrl) {
		this.fullProfileUrl = fullProfileUrl;
	}

	public String getCcpQualifier() {
		return ccpQualifier;
	}

	public void setCcpQualifier(String ccpQualifier) {
		this.ccpQualifier = ccpQualifier;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getParentGroupName() {
		return parentGroupName;
	}

	public void setParentGroupName(String parentGroupName) {
		this.parentGroupName = parentGroupName;
	}

	public String getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(String parentGroupId) {
		this.parentGroupId = parentGroupId;
	}

	public String getIntegredients() {
		return integredients;
	}

	public void setIntegredients(String integredients) {
		this.integredients = integredients;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCrops() {
		return crops;
	}

	public void setCrops(String crops) {
		this.crops = crops;
	}

	/**
	 * @return the customunits
	 */
	public List<UnitDeatilsTo> getCustomunits() {
		return customunits;
	}

	/**
	 * @param customunits the customunits to set
	 */
	public void setCustomunits(List<UnitDeatilsTo> customunits) {
		this.customunits = customunits;
	}

	/**
	 * @return the alsoKnownAs
	 */
	public String getAlsoKnownAs() {
		return alsoKnownAs;
	}

	/**
	 * @param alsoKnownAs the alsoKnownAs to set
	 */
	public void setAlsoKnownAs(String alsoKnownAs) {
		this.alsoKnownAs = alsoKnownAs;
	}

	/**
	 * @return the localeNames
	 */
	public List<LocaleNameTo> getLocaleNames() {
		return localeNames;
	}

	/**
	 * @param localeNames the localeNames to set
	 */
	public void setLocaleNames(List<LocaleNameTo> localeNames) {
		this.localeNames = localeNames;
	}

	/**
	 * @return the qualifiers
	 */
	public String getQualifiers() {
		return qualifiers;
	}

	/**
	 * @param qualifiers the qualifiers to set
	 */
	public void setQualifiers(String qualifiers) {
		this.qualifiers = qualifiers;
	}

	/**
	 * @return the commodityId
	 */
	public String getCommodityId() {
		return commodityId;
	}

	/**
	 * @param commodityId the commodityId to set
	 */
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}

	/**
	 * @return the commodityName
	 */
	public String getCommodityName() {
		return commodityName;
	}

	/**
	 * @param commodityName the commodityName to set
	 */
	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	/**
	 * @return the varietyId
	 */
	public String getVarietyId() {
		return varietyId;
	}

	/**
	 * @param varietyId the varietyId to set
	 */
	public void setVarietyId(String varietyId) {
		this.varietyId = varietyId;
	}

	/**
	 * @return the varietyName
	 */
	public String getVarietyName() {
		return varietyName;
	}

	/**
	 * @param varietyName the varietyName to set
	 */
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}

	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the baseUnitId
	 */
	public String getBaseUnitId() {
		return baseUnitId;
	}

	/**
	 * @param baseUnitId the baseUnitId to set
	 */
	public void setBaseUnitId(String baseUnitId) {
		this.baseUnitId = baseUnitId;
	}

	/**
	 * @return the baseUnitName
	 */
	public String getBaseUnitName() {
		return baseUnitName;
	}

	/**
	 * @param baseUnitName the baseUnitName to set
	 */
	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	/**
	 * @return the listofUnits
	 */
	public List<UnitDeatilsTo> getListofUnits() {
		return listofUnits;
	}

	/**
	 * @param listofUnits the listofUnits to set
	 */
	public void setListofUnits(List<UnitDeatilsTo> listofUnits) {
		this.listofUnits = listofUnits;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductTo other = (ProductTo) obj;
		if (productId == null) {
			if (other.productId != null)
				return equalsOverideWithVarietyId(obj);
		} else if (!productId.equals(other.productId))
			return equalsOverideWithVarietyId(obj);
		return true;
	}

	private boolean equalsOverideWithVarietyId(Object obj) {
		ProductTo other = (ProductTo) obj;
		if (varietyId == null) {
			if (other.varietyId != null)
				return false;
		} else if (!varietyId.equals(other.varietyId))
			return false;
		return true;
	}

	/**
	 * @return the localeName
	 */
	public String getLocaleName() {
		return localeName;
	}

	/**
	 * @param localeName the localeName to set
	 */
	public void setLocaleName(String localeName) {
		this.localeName = localeName;
	}

	/**
	 * @return the scientific_name
	 */
	public String getScientific_name() {
		return scientific_name;
	}

	/**
	 * @param scientific_name the scientific_name to set
	 */
	public void setScientific_name(String scientific_name) {
		this.scientific_name = scientific_name;
	}

	/**
	 * @return the weblinks
	 */
	public String getWeblinks() {
		return weblinks;
	}

	/**
	 * @param weblinks the weblinks to set
	 */
	public void setWeblinks(String weblinks) {
		this.weblinks = weblinks;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the universal_name
	 */
	public String getUniversal_name() {
		return universal_name;
	}

	/**
	 * @param universal_name the universal_name to set
	 */
	public void setUniversal_name(String universal_name) {
		this.universal_name = universal_name;
	}

	/**
	 * @return the productDisplayName
	 */
	public String getProductDisplayName() {
		return productDisplayName;
	}

	/**
	 * @param productDisplayName the productDisplayName to set
	 */
	public void setProductDisplayName(String productDisplayName) {
		this.productDisplayName = productDisplayName;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	@Override
	public int compareTo(ProductTo product) {
		return this.getProductName().compareTo(product.getProductName());
	}

	@Override
	public String toString() {
		return "ProductTo [productId=" + productId + ", productName=" + productName + ", baseUnitId=" + baseUnitId + ", baseUnitName=" + baseUnitName + ", commodityId=" + commodityId
				+ ", commodityName=" + commodityName + ", varietyId=" + varietyId + ", varietyName=" + varietyName + ", groupId=" + groupId + ", groupName=" + groupName + ", imageUrl=" + imageUrl
				+ ", alsoKnownAs=" + alsoKnownAs + ", productDescription=" + productDescription + ", qualifiers=" + qualifiers + ", localeNames=" + localeNames + ", listofUnits=" + listofUnits
				+ ", customunits=" + customunits + ", universal_name=" + universal_name + ", localeName=" + localeName + ", scientific_name=" + scientific_name + ", weblinks=" + weblinks + ", notes="
				+ notes + ", productDisplayName=" + productDisplayName + ", integredients=" + integredients + ", company=" + company + ", crops=" + crops + ", parentGroupId=" + parentGroupId
				+ ", parentGroupName=" + parentGroupName + ", productType=" + productType + ", parentIds=" + parentIds + ", ccpQualifier=" + ccpQualifier + "]";
	}

}