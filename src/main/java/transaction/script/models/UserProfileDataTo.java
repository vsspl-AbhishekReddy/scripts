package transaction.script.models;

import java.util.List;

public class UserProfileDataTo {
	private String firstName;
	private List<UserBusinessDetails> lstOfUserBusinessDetailsInfo;
	private String mobileNo;
	private String mobileCode;
	private String profileKey;
	private String prfCreatedDate;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public List<UserBusinessDetails> getLstOfUserBusinessDetailsInfo() {
		return lstOfUserBusinessDetailsInfo;
	}

	public void setLstOfUserBusinessDetailsInfo(List<UserBusinessDetails> lstOfUserBusinessDetailsInfo) {
		this.lstOfUserBusinessDetailsInfo = lstOfUserBusinessDetailsInfo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getProfileKey() {
		return profileKey;
	}

	public void setProfileKey(String profileKey) {
		this.profileKey = profileKey;
	}

	public String getPrfCreatedDate() {
		return prfCreatedDate;
	}

	public void setPrfCreatedDate(String prfCreatedDate) {
		this.prfCreatedDate = prfCreatedDate;
	}
}