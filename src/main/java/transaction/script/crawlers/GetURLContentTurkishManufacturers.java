package transaction.script.crawlers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class GetURLContentTurkishManufacturers {
	public static void main(String[] args) {
		String baseUrl = "http://www.turkish-manufacturers.com";
		for (int i = 1; i <= 14; i++) {
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL("http://www.turkish-manufacturers.com/turkey/bulk-cereal_page-" + i + ".html")));
				String[] text = document.getElementsByClass("title").html().split("</a>");
				for (String urlString : text) {
					String trimmedUrl = urlString.trim();
					String subLink = baseUrl + trimmedUrl.substring(9, trimmedUrl.indexOf("\" title"));
					Document subLinkDocument = Jsoup.parse(IOUtils.toString(new URL(subLink)));
					String bizName = subLinkDocument.getElementsByTag("h1").html();
					String addressWithPhone = subLinkDocument.getElementsByClass("adres").html();
					String address = addressWithPhone.substring(addressWithPhone.indexOf("Adress: </b>") + 12, addressWithPhone.indexOf("<br>")).trim();
					String phoneDetails = addressWithPhone.substring(addressWithPhone.indexOf("Phone: </b>") + 11);
					String phoneNumber = phoneDetails.substring(0, phoneDetails.indexOf("<b")).replace(" ", "").trim();
					String otherNumbers = phoneDetails.substring(phoneDetails.indexOf("Fax: </b>") + 9).replace(" ", "").trim();
					System.out.println(bizName + "\t||\t" + address + "\t||\t" + phoneNumber + "\t||\t" + otherNumbers);
					String productString = subLinkDocument.html().substring(subLinkDocument.html().indexOf("Products</h2>"));
					//System.out.println(productString);
					String prodcuts[] = productString.substring(productString.indexOf("<ul>") + 4, productString.indexOf("</ul")).replaceAll("<li>", "").replaceAll("</li>", ",")
							.split(",");
					String finalProductsString = "";
					for (String product : prodcuts) {
						product = product.trim();
						if (product != null && !product.isEmpty())
							finalProductsString += product + ",";
					}
					if (finalProductsString.endsWith(",")) {
						finalProductsString = finalProductsString.substring(0, finalProductsString.length() - 1);
					}
					System.out.println(finalProductsString);
					System.out.println("\n");
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}