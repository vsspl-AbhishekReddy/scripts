package transaction.script.crawlers.posts;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

public class GMDU {
	public static String domain = "https://www.gmdu.net";

	static Gson gson = new Gson();

	public static void main(String[] args) {
		int count = 0;
		for (int j = 1; j <= 51; j++) {
			try {
				Thread.sleep(60000);
				Document doc = Jsoup.connect("https://www.gmdu.net/trade-1-join-t0-p" + j + ".html")
						.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
				Elements text1 = doc.getElementsByClass("itemlist");
				ArrayList<String> postLinks = new ArrayList<String>();
				for (Element text : text1) {
					String postLink = text.getElementsByTag("a").attr("href");
					postLinks.add(postLink);
				}
				System.out.println(postLinks.size());
				for (int i = 0; i < postLinks.size(); i++) {
					System.out.println(postLinks.get(i));
					Document postDocument = Jsoup.connect(postLinks.get(i)).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
							.timeout(60000).get();
					Elements generalDetails = postDocument.getElementsByClass("general");
					Elements postDetail = postDocument.getElementsByClass("intro-item");
					Elements userDetails = postDocument.getElementsByClass("contact");
					Elements postProduct = postDocument.getElementsByTag("h1");
					String subject = "", postDate = "", expiryDate = "", description = "", companyName = "", address = "", postProductName = "", mobileNo = "", website = "", postType = "", country = "", category = "", name = "";
					description = postDetail.get(0).text();
					postDate = generalDetails.get(0).getElementsByClass("general-value").get(4).text();
					postType = generalDetails.get(0).getElementsByClass("general-value").get(0).text();
					country = generalDetails.get(0).getElementsByClass("general-value").get(1).text();
					category = generalDetails.get(0).getElementsByClass("general-value").get(2).text();
					companyName = userDetails.get(0).getElementsByClass("contact-value").get(0).text();
					address = userDetails.get(0).getElementsByClass("contact-value").get(2).text();
					mobileNo = userDetails.get(0).getElementsByClass("contact-value").get(1).text();
					name = userDetails.get(0).getElementsByClass("contact-value").get(0).text();
					postProductName = postProduct.get(0).text();
					PostToClass objPostToClass = TradeIndiaPosts.settingPostTo(postLinks.get(i), mobileNo, companyName, subject, description, postProductName, address, postDate, expiryDate, website);
					objPostToClass.setPostType(postType);
					objPostToClass.setCountryName(country);
					objPostToClass.setPostcategory(category);
					objPostToClass.setName(name);
					int result = TradeIndiaPosts.insertionDB(objPostToClass);
					if (result > 0) {
						System.out.println("success");
					} else {
						System.out.println("fail");
						System.out.println(gson.toJson(objPostToClass));
					}
					count++;
					System.out.println("count" + count + "" + j + "page no" + "link " + postLinks.get(i));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
