package transaction.script.crawlers.posts;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.gson.Gson;

public class FIJIN {
	static String baseUrl = "http://www.fijin.com";
	static Gson gson = new Gson();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			System.out.println("dfgfdghdfhdfh");
			Document doc = Jsoup.connect("http://www.fijin.com/catalog/Agriculture/")
					.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
			Elements dir = doc.getElementsByClass("dir");
			Elements categories = dir.get(0).getElementsByTag("a");
			ArrayList<String> categoriesLinks = new ArrayList<String>();
			for (Element category : categories) {
				String categoryLink = category.attr("href");
				categoriesLinks.add(categoryLink);
			}
			System.out.println(categoriesLinks.size());
			Document categoryDocument = null;
			for (int i = 0; i < categoriesLinks.size(); i++) {
				Thread.sleep(1000);
				System.out.println(baseUrl + categoriesLinks.get(i));
				try {
					categoryDocument = Jsoup.connect(baseUrl + categoriesLinks.get(i))
							.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
					Elements countrydir = categoryDocument.getElementsByClass("dir");
					Elements countries = countrydir.get(0).getElementsByTag("a");
					ArrayList<String> countriesLinks = new ArrayList<String>();
					for (Element country : countries) {
						String countryLink = country.attr("href");
						countriesLinks.add(countryLink);
					}
					System.out.println(countriesLinks.size());
					Document countryDocument = null;
					for (int j = 0; j < countriesLinks.size(); j++) {
						try {
							Document postDocument = null;
							String next=null;
							do {
								if(next==null||next.isEmpty()){
								countryDocument = Jsoup.connect(baseUrl + countriesLinks.get(j))
										.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
								System.out.println("@@@@@@@@@@@@@@@@@@");
								try {
									next=countryDocument.getElementById("pages").getElementsByTag("a").get(countryDocument.getElementById("pages").getElementsByTag("a").size()-1).attr("href");
								} catch (Exception e) {
									// TODO Auto-generated catch block
									System.out.println("no next button");
									next=null;
									e.printStackTrace();
									
								}
								}
								else
								{
									countryDocument = Jsoup.connect(baseUrl + next)
											.userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").timeout(60000).get();
									System.out.println("@@@@@@@@@@@@@@@@@@");
									try {
										next=countryDocument.getElementById("pages").getElementsByTag("a").get(countryDocument.getElementById("pages").getElementsByTag("a").size()-1).attr("href");
									} catch (Exception e) {
										// TODO Auto-generated catch block
										System.out.println("no next button");
										next=null;
										e.printStackTrace();
									}
								}
								Elements postLinks = countryDocument.getElementsByClass("search-post");
								for (Element postLink : postLinks) {
									String link = postLink.getElementsByTag("a").get(0).attr("href");
									System.out.println(link);
									postDocument = Jsoup.connect(baseUrl + link).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
											.timeout(60000).get();
									Elements postDetails = postDocument.getElementsByClass("display-post");
									Elements rows = postDetails.select("tr");
									HashMap<String, String> fields = new HashMap<String, String>();
									for (int k = 0; k < rows.size(); k++) {
										Element row = rows.get(k);
										Elements cols = row.select("td");
										String lable = cols.get(0).getElementsByClass("label").get(0).text();
										System.out.println(lable);
										String field = cols.get(1).getElementsByClass("field").get(0).text();
										System.out.println(field);
										fields.put(lable, field);
									}
									Element userDetils = postDocument.getElementById("display-profile");
									String companyName = "";
									String name = userDetils.getElementsByTag("a").get(0).text();
									String product = postDocument.getElementById("content").getElementsByTag("h1").get(0).text();
									if (userDetils.getElementsByTag("a").size() > 3) {
										companyName = userDetils.getElementsByTag("a").get(2).text();
									}
									PostToClass objPostToClass = settingTo(fields);
									objPostToClass.setProduct(product);
									objPostToClass.setLink(baseUrl + link);
									objPostToClass.setName(name);
									objPostToClass.setCompanyName(companyName);
									System.out.println(gson.toJson(objPostToClass));
								}
							} while (next==null||next.isEmpty());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static PostToClass settingTo(HashMap<String, String> fields){
		PostToClass objPostToClass=new PostToClass();
		for(String key:fields.keySet()){
			if(key.startsWith("Type")){
				objPostToClass.setPostType(fields.get(key));
			}
			if(key.startsWith("Category")){
				objPostToClass.setPostcategory(fields.get(key));
			}
			if(key.startsWith("City")){
				objPostToClass.setLocation(fields.get(key));
			}
			if(key.startsWith("Price")){
				objPostToClass.setPrice(fields.get(key));
			}
			if(key.startsWith("Address")){
				objPostToClass.setAddress(fields.get(key));
			}
			if(key.startsWith("Description")){
				objPostToClass.setDescription(fields.get(key));
			}
			if(key.startsWith("Date")){
				objPostToClass.setPostDate(fields.get(key));
			}
		}
		return objPostToClass;
		
	}
}
