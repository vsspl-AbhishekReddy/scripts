package transaction.script.crawlers.posts;

public class PostToClass {
	private String postDate;
	private String expiaryDate;
	private String description;
	private String subject;
	private String companyName;
	private String mobileNo;
	private String address;
	private String website;
	private String link;
	private String countryName;
	private String product;
	private String postType;
	private String postcategory;
	private String name;
	private String location;
	private String price;
	private String postRelatedInfo;
	private String userType;
	private String telephone;
	private String fax;

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPostRelatedInfo() {
		return postRelatedInfo;
	}

	public void setPostRelatedInfo(String postRelatedInfo) {
		this.postRelatedInfo = postRelatedInfo;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostcategory() {
		return postcategory;
	}

	public void setPostcategory(String postcategory) {
		this.postcategory = postcategory;
	}

	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getExpiaryDate() {
		return expiaryDate;
	}

	public void setExpiaryDate(String expiaryDate) {
		this.expiaryDate = expiaryDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	public static void main(String[] args) {
		String str="dfdsgdgfhf contact: rtgrtr";
		System.out.println(str.substring((str.indexOf("contact:")+"contact:".length()),str.indexOf("Company:")));
	}
}
