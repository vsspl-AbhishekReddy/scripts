package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Fijin {
	public static void main(String[] args) {
		String baseUrl = "http://www.fijin.com/";
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 0; i <= 3; i++) {
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL(
						"http://www.fijin.com/companies/Seafood/Nigeria-NG/"+i+"")));
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;

				try {
					Elements links = document.getElementsByClass("search-post-text");
					Document linkDocument = null;
					for (Element link : links) {
						String companyName = "",bizType ="", address = "", phoneNo = "", location = "", website = "", fax="";
						Row row = sheet.createRow(rowNumber);
						rowNumber++;
						//System.out.println(link.getElementsByTag("a").attr("href"));
						try {
							String text = link.getElementsByTag("a").attr(
									"href");
							linkDocument = Jsoup.parse(IOUtils
									.toString(new URL(baseUrl + text)));
							//System.out.println("hsfjhfgjh:::"+linkDocument);

							TimeUnit.MILLISECONDS.sleep(500);
							// System.out.println(baseUrl + text);
							companyName = linkDocument.getElementsByTag("h1")
									.text();
							//System.out.println(linkDocument);
							// System.out.println("\n\n\n" + companyName);
							Elements data = linkDocument
									.getElementsByClass("display-post");
							// System.out.println("dataaaaaaaaaaa:"+data);
							for (Element element : data) {
								for (Element element2 : element
										.getElementsByClass("label")) {
									if (element2.text().trim()
											.equals("Business category 1")) {
										bizType = element2
												.siblingElements().text();
									} else if (element2.text().trim()
											.equals("Address")) {
										address = element2.siblingElements()
												.text();
									} else if (element2.text().trim()
											.equals("Phones")) {
										phoneNo = element2.siblingElements()
												.text();
									} else if (element2.text().trim()
											.equals("City ( of main office )")) {
										location = element2.siblingElements()
												.text();
									}else if (element2.text().trim()
											.equals("Web site")) {
										website = element2.siblingElements()
												.text();
									}else if (element2.text().trim()
											.equals("Fax")) {
										fax = element2.siblingElements()
												.text();
									}
								}
							}
						} catch (Exception e) {
							continue;
						}
						row.createCell(0).setCellValue(companyName);
						row.createCell(1).setCellValue(bizType);
						row.createCell(2).setCellValue(address);
						row.createCell(3).setCellValue(phoneNo);
						row.createCell(4).setCellValue(location);
						row.createCell(5).setCellValue(website);
						row.createCell(6).setCellValue(fax);
					}

					System.out.println("page no::" + i + " row number"
							+ rowNumber);

				} catch (IndexOutOfBoundsException e) {
					break;
				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File(
					"/home/mahesh/Desktop/Nigeria_fijinSeaFood.xls"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}