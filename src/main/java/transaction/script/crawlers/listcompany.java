package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class listcompany {
	public static void main(String[] args) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 1; i <= 185; i++) {
			System.out.println("page count-----------------------"+i);
			try {
				Document document = Jsoup.parse(IOUtils.toString(new URL("http://www.listcompany.org/Agriculture_In_Turkey/p"+i+".html")));
				TimeUnit.MILLISECONDS.sleep(500);
				
				int index = 0;
				try {
					Elements links = document.getElementsByTag("h4");
					Document linkDocument= null;
					for (Element link : links){
				//while (true) {
					String Companyname  = "", products = "", address = "", mobile = "", otherMobile = "", bizName = "", website = "",CountryRegion="",
							Businesstype="",Location="",Contact_Person="";
					String phone="";
					Row row = sheet.createRow(rowNumber);
					rowNumber++;
						try {
							
								String text = link.getElementsByTag("a").attr(
										"href");								
								System.out.println("href-------------------"+text);
							
							

						
							try {
								linkDocument = Jsoup.parse(IOUtils.toString(new URL(text)));
	                             //System.out.println("linkDocument:::"+linkDocument);
							} catch (Exception e1) {
								///System.out.println(e1);
								e1.printStackTrace();
								continue;
							}
							TimeUnit.MILLISECONDS.sleep(500);
							//System.out.println(baseUrl + text);
							//Company name 
							Companyname= linkDocument.getElementsByTag("h1").text();
						    //System.out.println("header--------------------------------------"+Companyname);
						    
						    //country type 
							Elements country = linkDocument.getElementsByClass("information");
							//Elements country1 = country.tagName("li");
	 
	                        for(Element element : country){
	                        for(Element element2 :element.getElementsByTag("li")){
	                        	
	                         if(element2.text().trim().contains("Country/Region:")){
	                        	 CountryRegion = element2.tagName("span").text();
	                        	
	                        	// System.out.println("Country/Region:--------"+CountryRegion);
	                         }
	                         else if(element2.text().trim().contains("Address:"))
	                         {
	                        	 address = element2.tagName("span").text();
	                        	// System.out.println("address:--------"+address);

	                         } 
	                         else if(element2.text().trim().contains("Business Type:"))
	                         {
	                        	Businesstype = element2.tagName("span").text();
	                       	    //System.out.println("Businesstype:--------"+Businesstype);
	                         }  
	                         else if(element2.text().trim().contains("Main Products:"))
	                         {
	                        	products = element2.tagName("span").text();
	                       	    //System.out.println("products:--------"+products);

	                         } 
	                         else if(element2.text().trim().contains("Location:")){
	                        	 Location = element2.tagName("span").text();
	                       	    //System.out.println("Location:--------"+Location);

	                        }
	                         
	                      }
	                   }
									
							Elements second  = linkDocument.getElementsByClass("info");
							 for(Element element : second){
			                        for(Element element3 :element.getElementsByTag("li")){
			                        	  if(element3.text().trim().contains("Contact Person:")){
			                             	Contact_Person = element3.tagName("span").text();
			                             	 //System.out.println("Contact_Person:--------"+Contact_Person);
			                                }	
			                        	  else if(element3.text().trim().contains("Telephone:"))
			                        	  {
				                             	Elements elm= element3.tagName("span").getAllElements();
				                             	//System.out.println("elm :"+elm);
				                            Iterator<Element> itr =  elm.iterator();
				                            while (itr.hasNext()){
				                            	phone = itr.next().toString();
				                            }
				                            phone = phone.substring(11, phone.length()-2);
				                            phone = "http://www.listcompany.org/"+phone;
				                             	 //System.out.println("Telephone:--------"+phone);
										  }
			                        	  else if(element3.text().trim().contains("Mobilephone:"))
			                        	  {
			                        		  Elements elm= element3.tagName("span").getAllElements();
				                             	//System.out.println("elm :"+elm);
				                            Iterator<Element> itr =  elm.iterator();
				                            while (itr.hasNext()){
				                            	mobile = itr.next().toString();
				                            }
				                            mobile = mobile.substring(11, mobile.length()-2);
				                            mobile = "http://www.listcompany.org/"+mobile;
				                             	 //System.out.println("mobile:--------"+mobile);
										  }
										  }
			                        	  
			                        	
			                        
			                        }
						
							index++;
							row.createCell(0).setCellValue(Companyname);
							row.createCell(1).setCellValue(address);
							row.createCell(2).setCellValue(Businesstype);
							row.createCell(3).setCellValue(CountryRegion);
							row.createCell(4).setCellValue(Location);
							row.createCell(5).setCellValue(products);
							row.createCell(6).setCellValue(Contact_Person);
							row.createCell(7).setCellValue(phone);
							row.createCell(8).setCellValue(mobile);



						} catch (IndexOutOfBoundsException e) {
							break;
						
					}
					
					
				
				//}
				}
				} catch (Exception e) {
					// TODO: handle exception
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/vasudhaika/Desktop/Turkey/List of Agriculture Companies in Turkey_data.xls"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

