package transaction.script.crawlers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Allbiz {
	public static void main(String[] args) {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet();
		int rowNumber = 0;
		for (int i = 10; i <= 40; i++) {
			try {
				
				 Document doc = Jsoup.connect("http://www.ru.all.biz/en/equipment-for-livestock-breeding-epr1431?page="+i+"")
		                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
		                    .get();
				TimeUnit.MILLISECONDS.sleep(500);
				int index = 0;
			
					String Area = "", products = "Live stock", address = "", mobile = "", bizName = "", website = "";
		
					
					try {
						Elements text1 = doc.getElementsByClass("companies-list-name");
						Document linkDocument = null;
						for (Element element : text1)
						{
							Row row = sheet.createRow(rowNumber);
							rowNumber++;
						Elements text = text1.get(index).getElementsByTag("a");
						String subLink = text.attr("href");
						try {
							linkDocument = Jsoup.connect(subLink)
				                    .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
				                    .get();
						} catch (Exception e1) {
							continue;
						}
						TimeUnit.MILLISECONDS.sleep(500);
						bizName = linkDocument.getElementsByTag("h1").text();
						
						Area =linkDocument.getElementsByClass("b-pli-alliance__country").text();
						
						address = linkDocument.getElementsByAttributeValue("itemprop", "streetAddress").text();
						
						mobile= linkDocument.getElementsByClass("_number").text();
						
						website = linkDocument.getElementsByClass("b-pli-alliance__contacts__www").text();
						
						index++;
						System.out.println("Row :"+index+" Page:"+i);
						row.createCell(0).setCellValue(bizName);
						row.createCell(1).setCellValue(mobile);
						if (products.isEmpty())
							row.createCell(2).setCellValue(Area);
						else
							row.createCell(2).setCellValue(products);
						row.createCell(3).setCellValue(address);
						row.createCell(4).setCellValue(website);
						}
					
						
					} catch (IndexOutOfBoundsException e) {
						break;
					}
					
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (workbook != null) {
			try (FileOutputStream outputStream = new FileOutputStream(new File("/home/nandini/Desktop/Corpus/Russia/LiveStock_Allbiz_conti.xls"));) {
				workbook.write(outputStream);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}