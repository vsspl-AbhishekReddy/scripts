package transaction.script.excelSheet.validators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.constants.PlatformConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.PlaceTo;
import transaction.script.models.PostTo;
import transaction.script.models.ProductTo;
import transaction.script.models.ProfileAdditionalDetails;
import transaction.script.models.RegistrationTo;
import transaction.script.models.UnitDeatilsTo;
import transaction.script.platform.utils.HsqlDBUtil;
import transaction.script.platform.utils.PostgresDBUtil;
import transaction.script.platform.utils.SearchProductTo;

import com.google.gson.Gson;

public class ExcelSheetsValidatorForPosts {
	public static final Logger LOGGER = LoggerFactory.getLogger(ExcelSheetsValidatorForPosts.class);
	static ExcelSheetsValidatorForPosts classObj = new ExcelSheetsValidatorForPosts();
	static Client client = ClientBuilder.newClient();
	static Gson gson = new Gson();
	static Connection postgresConnection = null;
	static volatile Set<String> processedMobileNumbers;
	static volatile ArrayList<String> notFoundLocationsList = new ArrayList<>();
	static boolean initialDataSetupDone = false;
	static volatile Long currentDummyMobileNumber;
	static Connection hsqlConnection = null;
	static PreparedStatement preparedStatementDataSearchQuery = null;
	static PreparedStatement preparedStatementMobileSearchQuery = null;
	static PreparedStatement preparedStatementInsertIntoRegistrations = null;
	static PreparedStatement preparedStatementInsertIntoMaster = null;
	static PreparedStatement preparedStatementInsertIntoProfileUpdate = null;
	static PreparedStatement preparedStatementInsertIntoPosts = null;
	static PreparedStatement preparedStatementUpdateMaster = null;
	static PreparedStatement preparedStatementUpdatePosts = null;
	static HashMap<String, String> baseProducts = new HashMap<String, String>();
	static volatile HashMap<String, ProductTo> productsMap = new HashMap<String, ProductTo>();
	static volatile HashMap<String, PlaceTo> locationsMap = new HashMap<String, PlaceTo>();
	static {
		client.property(ClientProperties.CONNECT_TIMEOUT, 60000);
		client.property(ClientProperties.READ_TIMEOUT, 60000);
		postgresConnection = PostgresDBUtil.getConnection("users");
		hsqlConnection = HsqlDBUtil.dbConnection;
		classObj.fetchUniqueMobileNumbers();
		try {
			postgresConnection.setAutoCommit(false);
			preparedStatementDataSearchQuery = postgresConnection
					.prepareStatement("select * from dev_master where data->>'name' ilike ? and data->>'bizName' ilike ? and data->>'bizTypeId' = ? and data->>'locationId' = ?");
			preparedStatementMobileSearchQuery = postgresConnection.prepareStatement("select * from dev_master where mobile_number = ?");
			//Insert statements
			preparedStatementInsertIntoRegistrations = postgresConnection.prepareStatement("INSERT INTO registations1 (mobile_number,data) VALUES (?,CAST(? AS jsonb));");
			preparedStatementInsertIntoMaster = postgresConnection.prepareStatement("INSERT INTO dev_master (mobile_number,data) VALUES (?,CAST(? AS jsonb));");
			preparedStatementInsertIntoProfileUpdate = postgresConnection.prepareStatement("INSERT INTO user_additional (id,data) VALUES (?,CAST(? AS jsonb));");
			preparedStatementInsertIntoPosts = postgresConnection.prepareStatement("INSERT INTO precreated_posts (id,data) VALUES (?,CAST(? AS jsonb));");
			//Update statements
			preparedStatementUpdateMaster = postgresConnection.prepareStatement("update dev_master set mobile_number = ?, data = cast(? as jsonb) where mobile_number = ?");
			preparedStatementUpdatePosts = postgresConnection.prepareStatement("update precreated_posts set id = ? where id = ?");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		initialDataSetupDone = true;
	}

	public static void main(String[] args) {
		if (initialDataSetupDone == true) {
			try {
				String sheetNameWithLocation = "/home/abhi/Desktop/sheets/sellposts-agrixchange.xlsx";
				Workbook workbook = new XSSFWorkbook(new FileInputStream(sheetNameWithLocation));
				Sheet sheet = workbook.getSheet(AppConstants.DATA_SHEET);
				for (int rowNum = 0; rowNum < sheet.getLastRowNum() + 1; rowNum++) {
					System.out.println("User num::" + (rowNum + 1));
					String error = "";
					Row row = sheet.getRow(rowNum);
					try {
						RegistrationTo objRegistrationTo = new RegistrationTo();
						String countryName = "", products = "", description = "", countryId = "", postCreatedDate = "", postExpiryDate = "", productNameToId = "", productIdToQualifier = "", postType = "";
						HashMap<String, String> productNameToIdsMap = new HashMap<String, String>();
						HashMap<String, String> productIdToQualifiersMap = new HashMap<String, String>();
						boolean isInvalid = false, isPost = false, isEssentialUpdate = false;
						//Country Name
						if (row.getCell(11) != null && !row.getCell(11).toString().isEmpty()) {
							countryName = row.getCell(11).toString();
							countryId = AppConstants.COUNTRY_IDS.get(countryName);
						}
						if (countryId == null || countryName == null || countryId.isEmpty() || countryName.isEmpty()) {
							error = error.concat(AppConstants.INVALID_COUNTRY_NAME);
							row.createCell(17).setCellValue(error);
							LOGGER.info("Invalid country name for " + (rowNum + 1));
							continue;
						}
						//Name
						if (row.getCell(0) != null && !row.getCell(0).toString().isEmpty())
							objRegistrationTo.setName(row.getCell(0).toString());
						else {
							error = error.concat(AppConstants.INVALID_NAME);
							isInvalid = true;
							LOGGER.info("Invalid user name for " + (rowNum + 1));
						}
						//BizName
						if (row.getCell(1) != null && !row.getCell(1).toString().isEmpty())
							objRegistrationTo.setBusinessName(row.getCell(1).toString());
						else {
							error = error.concat(AppConstants.INVALID_BIZ_NAME);
							isInvalid = true;
							LOGGER.info("Invalid biz name for " + (rowNum + 1));
						}
						//Mobile Number
						if (row.getCell(2) != null && !row.getCell(2).toString().isEmpty())
							objRegistrationTo.setMobileNumber(validateMobileNumber(countryName, row.getCell(2).toString()));
						//Biz Type
						if (row.getCell(3) != null && !row.getCell(3).toString().isEmpty()) {
							ArrayList<String> bizTypeInfo = getBizTypeId(row.getCell(3).getStringCellValue().trim());
							if (bizTypeInfo != null) {
								objRegistrationTo.setBusinessTypeId(bizTypeInfo.get(0));
								objRegistrationTo.setBusinessTypeName(bizTypeInfo.get(1));
							} else {
								isInvalid = true;
								error = error.concat(AppConstants.INVALID_BIZ_TYPE);
								LOGGER.info("Invalid biz type name for " + (rowNum + 1));
							}
						} else {
							error = error.concat(AppConstants.INVALID_BIZ_TYPE);
							isInvalid = true;
							LOGGER.info("Invalid biz type name for " + (rowNum + 1));
						}
						//Location
						if (row.getCell(4) != null && !row.getCell(4).toString().isEmpty()) {
							String location = row.getCell(4).toString().toLowerCase().trim();
							if (notFoundLocationsList.contains(location)) {
								error = error.concat(AppConstants.INVALID_LOCATION);
								isInvalid = true;
								LOGGER.info("Invalid location name for " + (rowNum + 1));
							} else {
								PlaceTo objPlaceTo = null;
								if (locationsMap.get(location) == null) {
									objPlaceTo = getlocation(AppConstants.COUNTRY_IDS.get(countryName), location);
								} else
									objPlaceTo = locationsMap.get(location);
								if (objPlaceTo == null) {
									notFoundLocationsList.add(location);
									error = error.concat(AppConstants.INVALID_LOCATION);
									isInvalid = true;
									LOGGER.info("Invalid location name for " + (rowNum + 1));
								} else {
									locationsMap.put(objPlaceTo.getPlaceName().toLowerCase().trim(), objPlaceTo);
									objRegistrationTo.setLocationTo(objPlaceTo);
									objRegistrationTo.setApmcTo(objPlaceTo);
								}
							}
						} else {
							error = error.concat(AppConstants.INVALID_LOCATION);
							isInvalid = true;
							LOGGER.info("Invalid location name for " + (rowNum + 1));
						}
						//Products
						if (row.getCell(5) != null && !row.getCell(5).toString().isEmpty())
							products = row.getCell(5).toString();
						else {
							error = error.concat(AppConstants.INVALID_PRODUCT);
							isInvalid = true;
							LOGGER.info("Invalid products for " + (rowNum + 1));
						}
						//Email
						if (row.getCell(6) != null && !row.getCell(6).toString().isEmpty()) {
							objRegistrationTo.setEmail(row.getCell(6).toString());
							isEssentialUpdate = true;
						}
						//Website
						if (row.getCell(7) != null && !row.getCell(7).toString().isEmpty()) {
							objRegistrationTo.setWebsite(row.getCell(7).toString());
							isEssentialUpdate = true;
						}
						//Address
						if (row.getCell(8) != null && !row.getCell(8).toString().isEmpty()) {
							objRegistrationTo.setAddress(row.getCell(8).toString());
							isEssentialUpdate = true;
						}
						//Other phones
						if (row.getCell(9) != null && !row.getCell(9).toString().isEmpty()) {
							objRegistrationTo.setOtherPhoneNumbers(row.getCell(9).toString());
							isEssentialUpdate = true;
						}
						//Post description
						if (row.getCell(10) != null && !row.getCell(10).toString().isEmpty())
							description = row.getCell(10).toString();
						//Post created date
						if (row.getCell(12) != null && !row.getCell(12).toString().isEmpty())
							postCreatedDate = row.getCell(12).toString();
						//Post expiry date
						if (row.getCell(13) != null && !row.getCell(13).toString().isEmpty())
							postExpiryDate = row.getCell(13).toString();
						//Product Names to Ids
						if (row.getCell(14) != null && !row.getCell(14).toString().isEmpty()) {
							productNameToId = row.getCell(14).toString();
							for (String data : productNameToId.split(";")) {
								String temp[] = data.split(":");
								productNameToIdsMap.put(temp[0].toLowerCase().trim(), temp[1].toLowerCase().trim());
							}
						}
						//Prodcut Id to qualifiers
						if (row.getCell(15) != null && !row.getCell(15).toString().isEmpty()) {
							productIdToQualifier = row.getCell(15).toString();
							for (String data : productIdToQualifier.split(";")) {
								if (data != null && !data.isEmpty()) {
									String temp[] = data.split(":");
									productIdToQualifiersMap.put(temp[0].trim(), temp[1].toLowerCase().trim());
								}
							}
						}
						//Post Type
						if (row.getCell(16) != null && !row.getCell(16).toString().isEmpty()) {
							postType = row.getCell(16).toString();
							if (postType == null || postType.isEmpty()) {
								isInvalid = true;
								error = error.concat(AppConstants.INVALID_POST_TYPE);
								LOGGER.info("Invalid post type for " + (rowNum + 1));
							} else {
								isPost = true;
							}
						}
						if (!isInvalid) {
							objRegistrationTo.setName(objRegistrationTo.getName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ")
									.replace("+", " ").replace("{", " ").replace("}", " ").replace("*", " ").replace("?", " "));
							objRegistrationTo.setBusinessName(objRegistrationTo.getBusinessName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ")
									.replace("\\", " ").replace("+", " ").replace("{", " ").replace("}", " ").replace("*", " ").replace("?", " "));
							try {
								preparedStatementDataSearchQuery.setString(1, objRegistrationTo.getName());
								preparedStatementDataSearchQuery.setString(2, objRegistrationTo.getBusinessName());
								preparedStatementDataSearchQuery.setString(3, objRegistrationTo.getBusinessTypeId());
								preparedStatementDataSearchQuery.setString(4, objRegistrationTo.getLocationTo().getPlaceId());
								ResultSet dataResultSet = preparedStatementDataSearchQuery.executeQuery();
								MasterCollectionTo objDataMatchMasterCollectionTo = null;
								MasterCollectionTo objMobileMatchMasterCollectionTo = null;
								if (dataResultSet.next()) {
									objDataMatchMasterCollectionTo = gson.fromJson(dataResultSet.getString("data"), MasterCollectionTo.class);
									if (dataResultSet != null)
										dataResultSet.close();
								}
								//Actual Logic --------------------/////////////////////////----------------------------////////////////////////////--------------------------
								List<ProductTo> objProductToList = getProductDetailsFromData(products, productNameToIdsMap, countryId);
								if (objProductToList.size() < 1) {
									error = error.concat(AppConstants.INVALID_POST_PRODUCT);
									row.createCell(17).setCellValue(error);
									LOGGER.info("Invalid products for " + (rowNum + 1));
									continue;
								}
								if (isPost) {
									if (!objProductToList.isEmpty()) {
										if (objRegistrationTo.getMobileNumber() != null && !objRegistrationTo.getMobileNumber().isEmpty()) {
											preparedStatementMobileSearchQuery.setString(1, objRegistrationTo.getMobileNumber());
											ResultSet mobileResultSet = preparedStatementMobileSearchQuery.executeQuery();
											if (mobileResultSet.next()) {
												objMobileMatchMasterCollectionTo = gson.fromJson(mobileResultSet.getString("data"), MasterCollectionTo.class);
												if (mobileResultSet != null)
													mobileResultSet.close();
											}
										}
										ProfileAdditionalDetails objProfileAdditionalDetails = null;
										if (objMobileMatchMasterCollectionTo != null && objDataMatchMasterCollectionTo != null) {
											//Create post for the user with data match
											List<PostTo> objPostToList = generatePostObject(description, objProductToList, productIdToQualifiersMap, postType, postCreatedDate,
													postExpiryDate, objDataMatchMasterCollectionTo.getMobileNum());
											if (isEssentialUpdate)
												objProfileAdditionalDetails = generateUpdateProfileObject(objRegistrationTo, objDataMatchMasterCollectionTo, false);
											error = insertAllDataIntoPostgresql(null, objProfileAdditionalDetails, objPostToList, objDataMatchMasterCollectionTo.getMobileNum(),
													error, null);
										} else if (objMobileMatchMasterCollectionTo == null && objDataMatchMasterCollectionTo != null) {
											int length = objDataMatchMasterCollectionTo.getCountryCode().length();
											List<PostTo> objPostToList = null;
											if (objDataMatchMasterCollectionTo.getMobileNum().trim().substring(length).startsWith("000")
													&& objRegistrationTo.getMobileNumber() != null) {
												//update phone number in master with this number and create post for this user.
												//------------------/////////////////------------------- update phone number pending,,,,, after which there is no need to write generate post call in both places
												objProfileAdditionalDetails = generateUpdateProfileObject(objRegistrationTo, objDataMatchMasterCollectionTo, true);
												objPostToList = generatePostObject(description, objProductToList, productIdToQualifiersMap, postType, postCreatedDate,
														postExpiryDate, objRegistrationTo.getMobileNumber());
												error = insertAllDataIntoPostgresql(null, objProfileAdditionalDetails, objPostToList, objRegistrationTo.getMobileNumber(), error,
														objDataMatchMasterCollectionTo);
											} else {
												//create a post for the user with data match
												objPostToList = generatePostObject(description, objProductToList, productIdToQualifiersMap, postType, postCreatedDate,
														postExpiryDate, objDataMatchMasterCollectionTo.getMobileNum());
												if (isEssentialUpdate)
													objProfileAdditionalDetails = generateUpdateProfileObject(objRegistrationTo, objDataMatchMasterCollectionTo, false);
												error = insertAllDataIntoPostgresql(null, objProfileAdditionalDetails, objPostToList,
														objDataMatchMasterCollectionTo.getMobileNum(), error, null);
											}
										} else if (objMobileMatchMasterCollectionTo != null && objDataMatchMasterCollectionTo == null) {
											objRegistrationTo.setMobileNumber(getDummyMobileNumber(countryName));
											objRegistrationTo = generateRegistrationObject(objRegistrationTo, products, countryName, productNameToIdsMap, objProductToList);
											//Create a new user with the crawled data using the above dummy mobile number and then create a post for him.
											List<PostTo> objPostToList = generatePostObject(description, objProductToList, productIdToQualifiersMap, postType, postCreatedDate,
													postExpiryDate, objRegistrationTo.getMobileNumber());
											error = insertAllDataIntoPostgresql(objRegistrationTo, null, objPostToList, objRegistrationTo.getMobileNumber(), error, null);
										} else if (objMobileMatchMasterCollectionTo == null && objDataMatchMasterCollectionTo == null) {
											if (objRegistrationTo.getMobileNumber() == null)
												objRegistrationTo.setMobileNumber(getDummyMobileNumber(countryName));
											//Create user with dummy number and register him. To this new user create a post.
											objRegistrationTo = generateRegistrationObject(objRegistrationTo, products, countryName, productNameToIdsMap, objProductToList);
											List<PostTo> objPostToList = generatePostObject(description, objProductToList, productIdToQualifiersMap, postType, postCreatedDate,
													postExpiryDate, objRegistrationTo.getMobileNumber());
											error = insertAllDataIntoPostgresql(objRegistrationTo, null, objPostToList, objRegistrationTo.getMobileNumber(), error, null);
										}
									} else {
										error = error.concat(AppConstants.INVALID_POST_PRODUCT);
										LOGGER.info("Invalid products for " + (rowNum + 1));
									}
								}
								if (!isPost) {
									if (objDataMatchMasterCollectionTo == null) {
										objRegistrationTo = generateRegistrationObject(objRegistrationTo, products, countryName, productNameToIdsMap, objProductToList);
										error = insertAllDataIntoPostgresql(objRegistrationTo, null, null, objRegistrationTo.getMobileNumber(), error, null);
									} else {
										LOGGER.info("Data duplicate for " + (rowNum + 1));
										error = error.concat(AppConstants.INVALID_DATA);
									}
								}
							} catch (SQLException e) {
								e.printStackTrace();
								error = error.concat(e.getMessage() + "," + e.getCause());
							}
						}
						row.createCell(17).setCellValue(error);
						if (rowNum % 100 == 0) {
							{
								try {
									FileOutputStream fileOutputStream = new FileOutputStream(sheetNameWithLocation);
									workbook.write(fileOutputStream);
									FileInputStream fileInputStream = new FileInputStream(sheetNameWithLocation);
									workbook = new XSSFWorkbook(fileInputStream);
									sheet = workbook.getSheet(AppConstants.DATA_SHEET);
								} catch (FileNotFoundException e) {
									LOGGER.error("Unable to locate file");
									e.printStackTrace();
								} catch (IOException e) {
									LOGGER.error("Unable to load or write to file.");
									e.printStackTrace();
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						error = error.concat(e.getMessage() + "," + e.getCause());
						row.createCell(17).setCellValue(error);
					}
				}
				try {
					FileOutputStream fileOutputStream = new FileOutputStream(sheetNameWithLocation);
					workbook.write(fileOutputStream);
				} catch (FileNotFoundException e) {
					LOGGER.error("Unable to locate file");
					e.printStackTrace();
				} catch (IOException e) {
					LOGGER.error("Unable to load or write to file.");
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			LOGGER.error("Failed setting up initial data. Exiting....");
		}
	}

	private static String insertAllDataIntoPostgresql(RegistrationTo objRegistrationTo, ProfileAdditionalDetails objProfileAdditionalDetails, List<PostTo> objPostToList,
			String key, String error, MasterCollectionTo objDataMatchMasterCollectionTo) {
		try {
			if (objRegistrationTo != null) {
				preparedStatementInsertIntoMaster.setString(1, key);
				MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
				objMasterCollectionTo.setBizId(null);
				objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
				objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
				objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
				objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
				objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
				objMasterCollectionTo.setName(objRegistrationTo.getName());
				objMasterCollectionTo.setProfileId(null);
				objMasterCollectionTo.setPrfCreatedDate(null);
				objMasterCollectionTo.setStatus(AppConstants.IN_QUEUE);
				preparedStatementInsertIntoMaster.setString(2, gson.toJson(objMasterCollectionTo));
				preparedStatementInsertIntoRegistrations.setString(1, key);
				preparedStatementInsertIntoRegistrations.setString(2, gson.toJson(objRegistrationTo));
				preparedStatementInsertIntoMaster.executeUpdate();
				preparedStatementInsertIntoRegistrations.executeUpdate();
			}
			if (objProfileAdditionalDetails != null) {
				preparedStatementInsertIntoProfileUpdate.setString(1, key);
				preparedStatementInsertIntoProfileUpdate.setString(2, gson.toJson(objProfileAdditionalDetails));
				preparedStatementInsertIntoProfileUpdate.executeUpdate();
			}
			if (objPostToList != null && !objPostToList.isEmpty()) {
				int count = 0;
				for (PostTo postTo : objPostToList) {
					preparedStatementInsertIntoPosts.setString(1, key);
					preparedStatementInsertIntoPosts.setString(2, gson.toJson(postTo));
					preparedStatementInsertIntoPosts.addBatch();
					count++;
				}
				int updateCount[] = preparedStatementInsertIntoPosts.executeBatch();
				int total = 0;
				for (int i : updateCount) {
					total = total + i;
				}
				if (total < count)
					LOGGER.error("total is:" + total + " and number of rows is :" + count);
			}
			if (objDataMatchMasterCollectionTo != null) {
				preparedStatementUpdateMaster.setString(1, key);
				preparedStatementUpdateMaster.setString(2, gson.toJson(objDataMatchMasterCollectionTo));
				preparedStatementUpdateMaster.setString(3, objDataMatchMasterCollectionTo.getMobileNum());
				preparedStatementUpdatePosts.setString(1, key);
				preparedStatementUpdatePosts.setString(2, objDataMatchMasterCollectionTo.getMobileNum());
				preparedStatementUpdateMaster.executeUpdate();
				preparedStatementUpdatePosts.executeUpdate();
			}
			postgresConnection.commit();
			synchronized (processedMobileNumbers) {
				processedMobileNumbers.add(key);
			}
			error = error.concat("Success-");
		} catch (SQLException e) {
			e.printStackTrace();
			error = error.concat(e.getMessage());
			try {
				postgresConnection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return error;
	}

	public void fetchUniqueMobileNumbers() {
		processedMobileNumbers = new TreeSet<String>();//For removing duplicates and ordering
		try (PreparedStatement preparedStatement = postgresConnection.prepareStatement("Select mobile_number from dev_master");
				ResultSet resultSet = preparedStatement.executeQuery();) {
			while (resultSet.next()) {
				processedMobileNumbers.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static PlaceTo getlocation(String countryId, String subStrings) {
		String comparator = "";
		String spellcheckWord = subStrings;
		PlaceTo objPlaceTo = null;
		try {
			comparator = subStrings;
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
			spellcheckWord = java.net.URLEncoder.encode(spellcheckWord, "UTF-8");
			subStrings = subStrings.replaceAll("\\+", "%20AND%20");
			Response clientResponse = null;
			String URL = PlatformConstants.COMMONDATA_SEARCH
					+ "(filter:location%20AND%20countryId:"
					+ countryId
					+ ")%20AND%20"
					+ subStrings
					+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs%20locationLong%20stateName&start=0&rows=5000&spellcheck.q="
					+ spellcheckWord.replaceAll(" ", "%20OR%20");
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			if (responsedata.get("numFound").toString().equals("0")) {
				JSONObject temp = objJson.getJSONObject("spellcheck");
				JSONObject misSpellArray = null;
				JSONArray queryAndSuggestion = null;
				JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
				for (int i = 0; i < suggestionsArray.length(); i++) {
					if (suggestionsArray.get(i).toString().equalsIgnoreCase(comparator)) {
						misSpellArray = new JSONObject(suggestionsArray.get(i + 1).toString());
						queryAndSuggestion = misSpellArray.getJSONArray("suggestion");
					}
				}
				if (queryAndSuggestion != null) {
					subStrings = queryAndSuggestion.get(0).toString();
				} else {
					return objPlaceTo;
				}
				subStrings = subStrings.replaceAll(" ", "%20AND%20");
				URL = PlatformConstants.COMMONDATA_SEARCH
						+ "(filter:location%20AND%20countryId:"
						+ countryId
						+ ")%20AND%20"
						+ subStrings
						+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs%20locationLong%20stateName&start=0&rows=5000";
				webTarget = client.target(URL);
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.get();
				output = clientResponse.readEntity(String.class);
				objJson = new JSONObject(output);
				try {
					if (objJson.has("response")) {
						responsedata = objJson.getJSONObject("response");
					} else {
						return objPlaceTo;
					}
				} catch (Exception e) {
					LOGGER.error("Unable to find response");
				}
			}
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			for (int i = 0; i < array.length();) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				return gson.fromJson(obj1.get("_json_").toString(), PlaceTo.class);
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objPlaceTo;
	}

	public static List<ProductTo> getProductDetailsFromData(String products, HashMap<String, String> productNameToIdsMap, String countryId) {
		List<ProductTo> objProductToList = new ArrayList<ProductTo>();
		String notMappedProducts = "", mappedProducts = "";
		for (String productName : products.split(",")) {
			productName = productName.toLowerCase().trim();
			String productId = productNameToIdsMap.get(productName);
			if (productId != null) {
				if (productsMap.containsKey(productName)) {
					if (objProductToList.size() <= 10)
						objProductToList.add(productsMap.get(productName));
					else
						break;
				} else {
					if (mappedProducts.isEmpty())
						mappedProducts = mappedProducts.concat(productId);
					else
						mappedProducts = mappedProducts.concat(";" + productId);
				}
			} else {
				if (notMappedProducts.isEmpty())
					notMappedProducts = notMappedProducts.concat(productName);
				else
					notMappedProducts = notMappedProducts.concat("," + productName);
			}
		}
		if (objProductToList.size() <= 10 && !mappedProducts.isEmpty()) {
			try {
				List<ProductTo> productTos = getProductDetails(mappedProducts, countryId);
				if (productTos.size() > 0)
					objProductToList.addAll(productTos);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (objProductToList.size() <= 10 && !notMappedProducts.isEmpty()) {
			Set<ProductTo> objProductToSet = productsSearch(notMappedProducts, countryId);
			if (objProductToSet.size() > 0) {
				for (ProductTo productTo : objProductToSet) {
					if (productTo.getProductId() != null) {
						if (objProductToList.size() <= 10)
							objProductToList.add(productTo);
						if (!productsMap.containsKey(productTo.getProductName().toLowerCase().trim()))
							productsMap.put(productTo.getProductName().toLowerCase().trim(), productTo);
					}
				}
			}
		}
		return objProductToList;
	}

	public static Set<ProductTo> productsSearch(String subStrings, String countryId) {
		Set<ProductTo> productsset = new HashSet<ProductTo>();
		String productIds = "";
		try {
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = subStrings.replaceAll(",", " ");
			countryId = countryId.replace(",", " ");
			String URL = "";
			try {
				subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
				subStrings = subStrings.replaceAll("\\+", "%20");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			int rows = 5000;
			Response clientResponse = null;
			URL = PlatformConstants.COMMONDATA_SEARCH + "(countryId:(" + countryId + ")%20AND%20(" + subStrings + "*))&wt=json&spellcheck.dictionary=test&spellcheck=true&"
					+ "qf=%20alsoKnownAs%5E4" + "%20productName%5E5" + "%20localeNames.displayName%5E3" + "%20company1%20crops%5E2%20ingredients1&start=0&rows=" + rows
					+ "&fl=_json_";
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			if (obj.get("numFound").toString().equals("0")) {
				JSONObject temp = objJson.getJSONObject("spellcheck");
				JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
				if (suggestionsArray.length() == 0)
					return productsset;
				if (suggestionsArray.get(3).getClass().equals(JSONArray.class)) {
					JSONArray misSpellArray = new JSONArray(suggestionsArray.get(3).toString());
					JSONArray queryAndSuggestion = new JSONArray(misSpellArray.get(5).toString());
					subStrings = queryAndSuggestion.get(1).toString();
				} else {
					JSONObject object = new JSONObject(suggestionsArray.get(3).toString());
					JSONArray array1 = new JSONArray(object.getJSONArray("suggestion").toString());
					subStrings = array1.get(0).toString();
				}
				subStrings = subStrings.replaceAll("-", " ");
				subStrings = subStrings.replaceAll(" ", "%20AND%20");
				URL = PlatformConstants.COMMONDATA_SEARCH
						+ "(countryId:("
						+ countryId
						+ ")%20AND%20*"
						+ subStrings
						+ "*)&wt=json&spellcheck.dictionary=test&spellcheck=true&spellcheck.build=true&qf=productName%20alsoKnownAs%20localeNames.displayName%20company%20crops%20ingredients&start=0&rows=1500&fl=_json_";
				webTarget = client.target(URL);
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.get();
				output = clientResponse.readEntity(String.class);
				objJson = new JSONObject(output);
				responsedata = objJson.getJSONObject("response");
				result = responsedata.toString();
				obj = new JSONObject(result);
				test = obj.get("docs");
				array = new JSONArray(test.toString());
			}
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				SearchProductTo objSearchProductTo = gson.fromJson(obj1.get("_json_").toString(), SearchProductTo.class);
				if (objSearchProductTo.getVarietyId().subSequence(9, 12).equals("000")) {
					if (productsMap.containsKey(objSearchProductTo.getProductName().toLowerCase().trim())) {
						if (productsset.size() <= 10)
							productsset.add(productsMap.get(objSearchProductTo.getProductName().toLowerCase().trim()));
						else
							return productsset;
					} else {
						productIds = productIds.concat(objSearchProductTo.getProductId() + ";");
					}
				}
			}
			if (!productIds.isEmpty()) {
				List<ProductTo> productToList = getProductDetails(productIds.substring(0, productIds.length() - 1), countryId);
				if (productToList != null)
					productsset.addAll(productToList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return productsset;
	}

	public static List<ProductTo> getProductDetails(String productIds, String countryId) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProductTo> productToLst = new ArrayList<ProductTo>();
		List<String> productDupCheck = new ArrayList<String>();
		String[] listofIds = productIds.split(";");
		try {
			String query = createproductInQuery(null, productIds);
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, countryId);
			for (int i = 1; i <= listofIds.length; i++) {
				preparedStatement.setString(i + 1, listofIds[i - 1]);
			}
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				if (!productDupCheck.contains(resultSet.getString("vsspsc_code"))) {
					ProductTo objProductTo = new ProductTo();
					objProductTo.setProductId(resultSet.getString("vsspsc_code"));
					objProductTo.setProductName(resultSet.getString("display_name"));
					objProductTo.setBaseUnitId(resultSet.getInt("unit_id") + "");
					objProductTo.setBaseUnitName(resultSet.getString("unit_name"));
					objProductTo.setCommodityId(resultSet.getString("commodity_id"));
					objProductTo.setCommodityName(resultSet.getString("commodity"));
					objProductTo.setVarietyId(resultSet.getString("variety_id"));
					objProductTo.setVarietyName(resultSet.getString("variety"));
					objProductTo.setScientific_name(resultSet.getString("scientific_name"));
					objProductTo.setUniversal_name(resultSet.getString("universal_name"));
					objProductTo.setNotes(resultSet.getString("notes"));
					objProductTo.setWeblinks(resultSet.getString("weblinks"));
					objProductTo.setAlsoKnownAs(resultSet.getString("also_known_as"));
					objProductTo.setImageUrl(resultSet.getString("image"));
					objProductTo.setQualifiers(resultSet.getString("qualifiers"));
					objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
					objProductTo.setGroupId(resultSet.getString("group_id"));
					objProductTo.setGroupName(resultSet.getString("group_name"));
					objProductTo.setCrops(resultSet.getString("crops"));
					objProductTo.setCompany(resultSet.getString("company_id"));
					objProductTo.setIntegredients(resultSet.getString("ingredients"));
					UnitDeatilsTo details = new UnitDeatilsTo();
					details.setUnitId(resultSet.getInt("unit_id"));
					details.setUnitName(resultSet.getString("unit_name"));
					details.setConversion(resultSet.getDouble("conversion"));
					objProductTo.getListofUnits().add(details);
					objProductTo.setCustomunits(getProductCustomUnits(objProductTo.getProductId(), countryId));
					if (productToLst.size() <= 10)
						productToLst.add(objProductTo);
					else
						break;
					productDupCheck.add(objProductTo.getProductId());
					synchronized (productsMap) {
						productsMap.put(objProductTo.getProductName().toLowerCase().trim(), objProductTo);
					}
				}
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return productToLst;
	}

	public static String qualifiersort(String qualifier) {
		StringBuilder strbui = new StringBuilder();
		if ((qualifier != null) && !(qualifier.equals(""))) {
			String[] array = qualifier.split(",");
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				strbui.append(array[i]);
				if ((i + 1) < array.length) {
					strbui.append(",");
				}
			}
			qualifier = strbui.toString();
		}
		return qualifier;
	}

	private static String createproductInQuery(String sessionId, String productIds) {
		String query = " SELECT P.ingredients, P.crops, P.company_id,P.vsspsc_code, P.image,P.qualifiers, PL.display_name,P.commodity_id,P.commodity, P.variety_id,P.variety,PG.group_id,P.ALSO_KNOWN_AS,P.SCIENTIFIC_NAME,P.WEBLINKS,P.NOTES,"
				+ " P.UNIVERSAL_NAME,P.HSCODE, PG.group_name, U.unit_id, U.unit_name,U.conversion FROM Products P INNER JOIN Units U on P.unit_id = U.base_unit_id INNER JOIN PRODUCT_GROUPS PG on "
				+ " P.group_id = PG.group_id INNER JOIN PRODUCT_TO_COUNTRY PC on P.vsspsc_code = PC.vsspsc_code INNER JOIN PRODUCTS_LOCALE_NAMES PL on PL.vsspsc_code = PC.vsspsc_code WHERE PL.language_id =1 "
				+ " AND PC.country_id = PL.country_id AND PC.country_id = ? AND P.vsspsc_code IN (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < productIds.split(";").length; i++) {
			queryBuilder.append(" ?");
			if (i != productIds.split(";").length - 1) {
				queryBuilder.append(",");
			}
		}
		queryBuilder.append(") ORDER BY PL.display_name ASC");
		return queryBuilder.toString();
	}

	private static List<UnitDeatilsTo> getProductCustomUnits(String vsspscCode, String countryId) {
		PreparedStatement preparedStatement = null;
		List<UnitDeatilsTo> customUnitsList = new ArrayList<>();
		ResultSet resultSet = null;
		String query = "SELECT custom_unitid, custom_unitname, conversion FROM PRODUCTS_TO_CUSTOMUNITS PC" + " INNER JOIN CUSTOMUNITS C on PC.custom_unitid = C.custom_unitid"
				+ " WHERE PC.vsspsc_code = ? AND C.country_id = ?";
		try {
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, vsspscCode);
			preparedStatement.setString(2, countryId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				UnitDeatilsTo objUnitDeatilsTo = new UnitDeatilsTo();
				objUnitDeatilsTo.setUnitId(resultSet.getInt("custom_unitid"));
				objUnitDeatilsTo.setUnitName(resultSet.getString("custom_unitname"));
				objUnitDeatilsTo.setConversion(resultSet.getDouble("conversion"));
				customUnitsList.add(objUnitDeatilsTo);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			LOGGER.error("Problem while fetching Custom units...");
		}
		Collections.sort(customUnitsList, new Comparator<UnitDeatilsTo>() {
			@Override
			public int compare(UnitDeatilsTo o1, UnitDeatilsTo o2) {
				String placeName1 = o1.getUnitName();
				String placeName2 = o2.getUnitName();
				// ascending order
				return placeName1.compareTo(placeName2);
			}

		});
		return customUnitsList;
	}

	private static synchronized String getDummyMobileNumber(String countryName) {
		if (currentDummyMobileNumber == null) {
			try (Statement stmt = hsqlConnection.createStatement(); ResultSet resultSet = stmt.executeQuery("SELECT CURRENTNUMBER FROM DUMMY_NUMBERS;")) {
				while (resultSet.next()) {
					currentDummyMobileNumber = (long) resultSet.getInt("CURRENTNUMBER");
				}
			} catch (Exception e) {
				LOGGER.error("Error in fetching dummy mobile number");
				e.printStackTrace();
			}
		}
		String dummyNumber;
		synchronized (processedMobileNumbers) {
			do {
				currentDummyMobileNumber++;
				dummyNumber = currentDummyMobileNumber.toString();
				while (dummyNumber.length() != 10) {
					dummyNumber = "0" + dummyNumber;
				}
				String mobileTelecomCode = AppConstants.COUNTRY_CODES.get(countryName);
				dummyNumber = mobileTelecomCode + dummyNumber;
			} while (processedMobileNumbers.contains(dummyNumber));
			updateDummyMobileNumber();
		}
		return dummyNumber;
	}

	private static void updateDummyMobileNumber() {
		try (PreparedStatement prepStmt = hsqlConnection.prepareStatement("UPDATE DUMMY_NUMBERS SET CURRENTNUMBER = " + currentDummyMobileNumber + ";")) {
			prepStmt.executeUpdate();
		} catch (Exception e) {
			LOGGER.error("Error in updating dummy number.");
			e.printStackTrace();
		}
	}

	private static String validateMobileNumber(String countryName, String mobileNumber) {
		String[] validMobileNumberLengths = AppConstants.MOBILE_TELECOM_CODES_LENGTH_MAP.get(AppConstants.COUNTRY_CODES.get(countryName)).split(",");
		boolean isValidNumber = false;
		for (String length : validMobileNumberLengths) {
			if (mobileNumber.length() == Integer.valueOf(length))
				isValidNumber = true;
		}
		String mobileTelecomCode = AppConstants.COUNTRY_CODES.get(countryName);
		String mobileNumberWithTelecomeCode = mobileTelecomCode + mobileNumber;
		if (isValidNumber)
			return mobileNumberWithTelecomeCode;
		else
			return null;
	}

	private static List<PostTo> generatePostObject(String description, List<ProductTo> objProductToList, HashMap<String, String> productIdToQualifiersMap, String postType,
			String postCreatedDate, String postExpiryDate, String mobileNum) {
		List<PostTo> objPostToList = new ArrayList<PostTo>();
		for (ProductTo objProductTo : objProductToList) {
			PostTo objPostTo = new PostTo();
			objPostTo.setAttachments("");
			objPostTo.setAudioURL("");
			objPostTo.setCurrencyCode("");
			objPostTo.setExpressInterest(false);
			objPostTo.setIsprivate(false);
			objPostTo.setMessage(description);
			objPostTo.setMobileNum(mobileNum);
			objPostTo.setOfferprice("");
			objPostTo.setOfferPriceUnitsid("");
			if (postCreatedDate != null && !postCreatedDate.isEmpty())
				objPostTo.setPostCreatedDate(parseDate(postCreatedDate));
			if (postExpiryDate != null && !postExpiryDate.isEmpty())
				objPostTo.setPostExpiryDate(parseDate(postExpiryDate));
			objPostTo.setProductid(objProductTo.getProductId());
			objPostTo.setProductName(objProductTo.getProductName());
			objPostTo.setQualifiers(productIdToQualifiersMap.get(objProductTo.getProductId()));
			objPostTo.setQuantity("");
			if (postType.equalsIgnoreCase(AppConstants.BUY_POST)) {
				objPostTo.setThreadCategory("1");
				if (objPostTo.getQualifiers() != null && !objPostTo.getQualifiers().isEmpty())
					objPostTo.setSubject("Want to buy " + objPostTo.getQualifiers() + " " + objPostTo.getProductName());
				else
					objPostTo.setSubject("Want to buy " + objPostTo.getProductName());
			} else {
				objPostTo.setThreadCategory("2");
				if (objPostTo.getQualifiers() != null && !objPostTo.getQualifiers().isEmpty())
					objPostTo.setSubject("Want to sell " + objPostTo.getQualifiers() + " " + objPostTo.getProductName());
				else
					objPostTo.setSubject("Want to sell " + objPostTo.getProductName());
			}
			objPostTo.setTxtFilesattachments("");
			objPostTo.setUnitsid("");
			objPostTo.setStatus(AppConstants.IN_QUEUE);
			objPostTo.setIdentifierTimestamp(String.valueOf(System.currentTimeMillis()));
			try {
				TimeUnit.MILLISECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			objPostToList.add(objPostTo);
		}
		return objPostToList;
	}

	private static String parseDate(String date) {
		DateFormat format = new SimpleDateFormat("MMM dd yyyy");
		DateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		dbFormat.setLenient(false);
		try {
			return dbFormat.format(format.parse(date.trim()));
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static RegistrationTo generateRegistrationObject(RegistrationTo objRegistrationTo, String products, String countryName, HashMap<String, String> productNameToIdsMap,
			List<ProductTo> objProductToList) {
		if (processedMobileNumbers.contains(objRegistrationTo.getMobileNumber()))
			objRegistrationTo.setMobileNumber(getDummyMobileNumber(countryName));
		objRegistrationTo.setMobileTelecomCode(AppConstants.COUNTRY_CODES.get(countryName));
		objRegistrationTo.setIsPreCreated(true);
		objRegistrationTo.setStatus(AppConstants.IN_QUEUE);
		objRegistrationTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceName());
		objRegistrationTo.setLstOfProducts(objProductToList);
		return objRegistrationTo;
	}

	private static ProfileAdditionalDetails generateUpdateProfileObject(RegistrationTo objRegistrationTo, MasterCollectionTo objDataMatchMasterCollectionTo, boolean isMobileUpdate) {
		ProfileAdditionalDetails objProfileAdditionalDetails = new ProfileAdditionalDetails();
		objProfileAdditionalDetails.setEmail(objRegistrationTo.getEmail());
		objProfileAdditionalDetails.setMobileNumber(objDataMatchMasterCollectionTo.getMobileNum());
		if (isMobileUpdate)
			objProfileAdditionalDetails.setNewMobileNumber(objRegistrationTo.getMobileNumber());
		objProfileAdditionalDetails.setOtherAddress(objRegistrationTo.getAddress());
		objProfileAdditionalDetails.setPhoneOthers(objRegistrationTo.getOtherPhoneNumbers());
		objProfileAdditionalDetails.setStatus(AppConstants.IN_QUEUE);
		objProfileAdditionalDetails.setWebsite(objRegistrationTo.getWebsite());
		return objProfileAdditionalDetails;
	}

	private static ArrayList<String> getBizTypeId(String bizTypeName) {
		ArrayList<String> bizTypeInfo = new ArrayList<String>();
		bizTypeName = bizTypeName.toLowerCase();
		switch (bizTypeName) {
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRITRADER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRITRADER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_AGRITRADER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIBROKER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIBROKER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_BROKER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_FARMER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_FARMER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_FARMER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_TRANSPORTER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_TRANSPORTER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_TRANSPORTER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIINPUT:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIINPUT);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_INPUTS);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRISOCIETY:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRISOCIETY);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_SOCIETY);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_WAREHOUSE:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_WAREHOUSE);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_WAREHOUSE);
			return bizTypeInfo;
		default:
			return null;
		}
	}
}