package transaction.script.excelSheet.validators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.constants.PlatformConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.PlaceTo;
import transaction.script.models.ProductTo;
import transaction.script.models.RegistrationTo;
import transaction.script.models.UnitDeatilsTo;
import transaction.script.platform.utils.HsqlDBUtil;
import transaction.script.platform.utils.PostgresDBUtil;
import transaction.script.platform.utils.SearchProductTo;

import com.google.gson.Gson;

public class ExcelSheetsValidator extends Thread {
	public static final Logger LOGGER = LoggerFactory.getLogger(ExcelSheetsValidator.class);
	private static final int maxThreads = 6;
	private static volatile int currentlyRunningThreads = 0;
	static Client client = ClientBuilder.newClient();
	/*static DB db = null;
	static DBCollection dumpingCollection = null;
	static DBCollection masterCollection = null;*/
	static Gson gson = null;
	static Connection hsqlConnection = null;
	static volatile HashMap<String, ProductTo> productsMap = new HashMap<String, ProductTo>();
	static volatile HashMap<String, PlaceTo> locationsMap = new HashMap<String, PlaceTo>();
	static volatile ArrayList<String> notFoundLocationsList = new ArrayList<>();
	static volatile ArrayList<String> notFoundProductsList = new ArrayList<>();
	static volatile Set<String> processedMobileNumbers;
	static volatile Long currentDummyMobileNumber;
	static Pattern pattern = Pattern.compile("[^a-z A-Z \\s]*");
	static volatile HashMap<String, String> sheetDetails = new HashMap<String, String>();
	static volatile boolean shouldICleanUpAndStop = false;
	static volatile int initialThreadCount = 0;
	static Connection connection = null;
	static {
		try {
			client.property(ClientProperties.CONNECT_TIMEOUT, 60000);
			client.property(ClientProperties.READ_TIMEOUT, 60000);
			hsqlConnection = HsqlDBUtil.dbConnection;
			/*db = MongoDBUtil.getConnection();
			dumpingCollection = db.getCollection("OCT20");// chandutest
			masterCollection = db.getCollection("masterCollectionOCT20");// masterchandutest
			*/gson = new Gson();
			connection = PostgresDBUtil.getConnection("users");
		} catch (Exception e) {
			LOGGER.error("Unable to initialise scripts");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void fetchUniqueMobileNumbers() {
		processedMobileNumbers = new TreeSet<String>();//For removing duplicates
		try (PreparedStatement preparedStatement = connection.prepareStatement("Select mobile_number from master5"); ResultSet resultSet = preparedStatement.executeQuery();) {
			while (resultSet.next()) {
				processedMobileNumbers.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}

		/*while (true) {
			DBCursor cursor = masterCollection.find().skip(offset).limit(5000);
			if (cursor.size() > 0) {
				while (cursor.hasNext()) {
					MasterCollectionTo masterCollectionTo = gson.fromJson(cursor.next().toString(), MasterCollectionTo.class);
					if (masterCollectionTo.getMobileNum() != null)
						processedMobileNumbers.add(masterCollectionTo.getMobileNum());
				}
				offset += 5000;
			} else {
				break;
			}
		}*/
	}

	public static void main(String[] args1) {
		LOGGER.warn("Please wait setting up required data......");
		ExcelSheetsValidator classObj = new ExcelSheetsValidator();
		int i = 1;
		initialThreadCount = Thread.activeCount();
		LOGGER.info("initial thread count is ::" + initialThreadCount);
		fetchUniqueMobileNumbers();
		System.out.println(processedMobileNumbers.size());
		LOGGER.warn("Data setup done...... Hulk ready to smash!!");
		System.out.println("Enter sheet name with location::");
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextLine()) {
			String input = scanner.nextLine();
			if (input.equalsIgnoreCase("exit")) {
				scanner.close();
				System.exit(0);
			}
			if (input.equalsIgnoreCase("clean")) {
				shouldICleanUpAndStop = true;
			} else {
				sheetDetails.put("ThreadID:" + i, input);
				Thread thread = new Thread(classObj, "ThreadID:" + i);
				thread.start();
				i++;
			}
			System.out.println("Type exit to close the program or sheet name with location to process new sheet::");
		}
		scanner.close();

	}

	public void run() {
		String threadName = Thread.currentThread().getName();
		LOGGER.info(threadName + " Hulk has started processing :" + sheetDetails.get(threadName));
		while (currentlyRunningThreads >= maxThreads) {
			LOGGER.info("Suspending " + sheetDetails.get(threadName) + " for 10 mins due to over load...");
			LOGGER.info("Current number of sheets which are suspended are ::" + (Thread.activeCount() - initialThreadCount - currentlyRunningThreads));
			try {
				TimeUnit.MINUTES.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			LOGGER.info("\n\n\n Current number of active threads ::" + Thread.activeCount());
			System.out.println("initial thread count is ::" + initialThreadCount);
		}
		currentlyRunningThreads++;
		validate(sheetDetails.get(threadName), threadName);
		LOGGER.info("\n\n\n\n" + threadName + " Hulk has finished processing :" + sheetDetails.get(threadName));
		Thread.currentThread().interrupt();
	}

	public static void validate(String sheetNameWithLocation, String threadName) {
		Connection threadConnection = null;
		try {
			threadConnection = PostgresDBUtil.getConnection("users");
			threadConnection.setAutoCommit(false);
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		PreparedStatement threadPreparedStatementMaster = null;
		PreparedStatement threadPreparedStatementRegistrations = null;
		PreparedStatement preparedStatementSearchQuery = null;
		try {
			threadPreparedStatementMaster = threadConnection.prepareStatement("INSERT INTO master5 (mobile_number,data) VALUES (?,CAST(? AS jsonb));");
			threadPreparedStatementRegistrations = threadConnection.prepareStatement("INSERT INTO registations2 (mobile_number,data) VALUES (?,CAST(? AS jsonb));");
			preparedStatementSearchQuery = threadConnection
					.prepareStatement("select * from master5 where data->>'name' ilike ? and data->>'bizName' ilike ? and data->>'bizTypeId' = ? and data->>'locationId' = ?");
		} catch (SQLException e1) {
			LOGGER.info("Unable to create database statement properly. Interupting current thread.");
			e1.printStackTrace();
			do {
				Thread.currentThread().interrupt();
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} while (Thread.currentThread().isInterrupted());
		}
		String fileName = sheetNameWithLocation.substring(sheetNameWithLocation.lastIndexOf("/") + 1);
		Workbook workbook = null;
		try (FileInputStream inputStream = new FileInputStream(sheetNameWithLocation);) {
			workbook = new XSSFWorkbook(inputStream);
			Sheet dataSheet = workbook.getSheet(AppConstants.DATA_SHEET);
			if (dataSheet != null) {
				String countryName = fileName.split("_")[0];
				if (AppConstants.COUNTRY_IDS.get(countryName) == null) {
					LOGGER.error("Country Not Mapped.");
					return;
				}
				long startTime = System.currentTimeMillis();
				for (int i = 1, count = 1; i <= dataSheet.getLastRowNum() && !shouldICleanUpAndStop; i++, count++) {
					try {
						validateAndProcessData(countryName, dataSheet, i, threadName, threadPreparedStatementMaster, threadPreparedStatementRegistrations,
								preparedStatementSearchQuery, threadConnection);
						if (i % 100 == 0) {
							{
								try {
									FileOutputStream fileOutputStream = new FileOutputStream(sheetNameWithLocation);
									workbook.write(fileOutputStream);
									FileInputStream fileInputStream = new FileInputStream(sheetNameWithLocation);
									workbook = new XSSFWorkbook(fileInputStream);
									dataSheet = workbook.getSheet(AppConstants.DATA_SHEET);
								} catch (FileNotFoundException e) {
									LOGGER.error("Unable to locate file");
									e.printStackTrace();
								} catch (IOException e) {
									LOGGER.error("Unable to load or write to file.");
									e.printStackTrace();
								}
							}
						}
					} catch (SQLException e) {
						e.printStackTrace();
						dataSheet.getRow(i).createCell(10).setCellValue(e.getMessage());
						LOGGER.error(threadName + "::Exception occurred for record number:" + i + " Message is :: " + e.getMessage());
						try {
							threadConnection.rollback();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					} catch (Exception e) {
						dataSheet.getRow(i).createCell(10).setCellValue(e.getMessage());
						e.printStackTrace();
						LOGGER.error(threadName + "::Exception occurred for record number:" + i + " Message is :: " + e.getMessage());
					}
					if (count != 0 && count % 100 == 0) {
						LOGGER.info(threadName + "::Hulk status is :: Processed " + count + " records in " + (System.currentTimeMillis() - startTime) + " ms");
						LOGGER.info(threadName + "::Containing " + productsMap.size() + " products details in map currently");
						LOGGER.info(threadName + "::Containing " + locationsMap.size() + " location details in map currently");
					}
				}
				LOGGER.info(threadName + "::Total time taken to process sheet is::" + (System.currentTimeMillis() - startTime) + " ms");
			} else {
				LOGGER.error(threadName + "::DATA sheet is not present. Program exiting....");
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Unable to locate file");
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("Unable to load or write to file.");
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error("Major exception...");
			e.printStackTrace();
		} finally {
			currentlyRunningThreads--;
			try {
				if (threadPreparedStatementMaster != null)
					threadPreparedStatementMaster.close();
				if (threadPreparedStatementRegistrations != null)
					threadPreparedStatementRegistrations.close();
				if (threadConnection != null)
					threadConnection.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			if (workbook != null) {
				try (FileOutputStream outputStream = new FileOutputStream(sheetNameWithLocation);) {
					workbook.write(outputStream);
				} catch (FileNotFoundException e) {
					LOGGER.error("Unable to locate file");
					e.printStackTrace();
				} catch (IOException e) {
					LOGGER.error("Unable to load or write to file.");
					e.printStackTrace();
				}
			}
		}
	}

	private static void validateAndProcessData(String countryName, Sheet dataSheet, int rowNumber, String threadName, PreparedStatement threadPreparedStatementMaster,
			PreparedStatement threadPreparedStatementRegistrations, PreparedStatement preparedStatementSearchQuery, Connection threadConnection) throws SQLException {
		RegistrationTo objRegistrationTo = new RegistrationTo();
		String errorMessage = "";
		boolean inValidFound = false;
		Row row = dataSheet.getRow(rowNumber);
		// Name
		if (row.getCell(0) != null) {
			if (!row.getCell(0).getStringCellValue().trim().equals(""))
				objRegistrationTo.setName(row.getCell(0).getStringCellValue().trim());
			else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
		}
		// BizName
		if (row.getCell(1) != null) {
			if (!row.getCell(1).getStringCellValue().trim().equals(""))
				objRegistrationTo.setBusinessName(row.getCell(1).getStringCellValue().trim());
			else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
		}
		// BizTypeName
		if (row.getCell(3) != null) {
			ArrayList<String> bizTypeInfo = getBizTypeId(row.getCell(3).getStringCellValue().trim());
			if (bizTypeInfo != null) {
				objRegistrationTo.setBusinessTypeId(bizTypeInfo.get(0));
				objRegistrationTo.setBusinessTypeName(bizTypeInfo.get(1));
			} else {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
		}
		// Location
		String locationName = row.getCell(4).getStringCellValue().trim();
		if (row.getCell(4) != null) {
			PlaceTo objPlaceTo = locationsMap.get(locationName);
			if (objPlaceTo != null) {
				objRegistrationTo.setLocationTo(objPlaceTo);
				objRegistrationTo.setApmcTo(objPlaceTo);
			}
			if (objPlaceTo == null && !notFoundLocationsList.contains(locationName) || notFoundLocationsList.size() < 1) { //
				objPlaceTo = getlocation(AppConstants.COUNTRY_IDS.get(countryName), locationName);
				if (objPlaceTo != null) {
					synchronized (locationsMap) {
						locationsMap.put(locationName, objPlaceTo);
					}
					objRegistrationTo.setLocationTo(objPlaceTo);
					objRegistrationTo.setApmcTo(objPlaceTo);
				} else {
					inValidFound = true;
					errorMessage = errorMessage.concat(AppConstants.INVALID_LOCATION);
					notFoundLocationsList.add(locationName);
				}
			} else if (objPlaceTo == null) {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_LOCATION);
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_LOCATION);
		}
		// Products
		if (row.getCell(5) != null) {
			String productString = row.getCell(5).getStringCellValue();
			ArrayList<ProductTo> productToList = new ArrayList<ProductTo>();
			if (notFoundProductsList.contains(productString)) {
				inValidFound = true;
				errorMessage = errorMessage.concat(AppConstants.INVALID_PRODUCT);
			} else {
				Set<ProductTo> productsSet = productsSearch(productString, AppConstants.COUNTRY_IDS.get(countryName));
				if (productsSet.size() > 0) {
					for (ProductTo productTo : productsSet) {
						if (productToList.size() <= 10)
							productToList.add(productTo);
						else
							break;
					}
					objRegistrationTo.setLstOfProducts(productToList);
				} else {
					notFoundProductsList.add(productString);
					inValidFound = true;
					errorMessage = errorMessage.concat(AppConstants.INVALID_PRODUCT);
				}
			}
		} else {
			inValidFound = true;
			errorMessage = errorMessage.concat(AppConstants.INVALID_PRODUCT);
		}
		// Email
		if (row.getCell(6) != null) {
			objRegistrationTo.setEmail(row.getCell(6).getStringCellValue());
		}
		// Website
		if (row.getCell(7) != null)
			objRegistrationTo.setWebsite(row.getCell(7).getStringCellValue());
		// Address
		if (row.getCell(8) != null)
			objRegistrationTo.setAddress(row.getCell(8).getStringCellValue());
		// Other contact numbers
		if (row.getCell(9) != null)
			objRegistrationTo.setOtherPhoneNumbers(row.getCell(9).getStringCellValue());
		if (!inValidFound) {
			String name = objRegistrationTo.getName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ")
					.replace("{", " ").replace("}", " ").replace("*", " ").replace("?", " ");
			String bizName = objRegistrationTo.getBusinessName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ")
					.replace("{", " ").replace("}", " ").replace("*", " ").replace("?", " ");
			/*List<BasicDBObject> list = new ArrayList<BasicDBObject>();
			list.add(new BasicDBObject("name", Pattern.compile("^" + name + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizName", Pattern.compile("^" + bizName + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizTypeId", objRegistrationTo.getBusinessTypeId()));
			list.add(new BasicDBObject("locationId", objRegistrationTo.getLocationTo().getPlaceId()));
			DBCursor masterCollectionDbCursor = masterCollection.find(new BasicDBObject("$and", list));*/
			preparedStatementSearchQuery.setString(1, name);
			preparedStatementSearchQuery.setString(2, bizName);
			preparedStatementSearchQuery.setString(3, objRegistrationTo.getBusinessTypeId());
			preparedStatementSearchQuery.setString(4, objRegistrationTo.getLocationTo().getPlaceId());
			ResultSet resultSet = preparedStatementSearchQuery.executeQuery();
			if (resultSet.next()) {
				try {
					if (resultSet != null)
						resultSet.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				errorMessage = errorMessage.concat(AppConstants.INVALID_DATA);
				row.createCell(10).setCellValue(errorMessage);
			} else {
				try {
					if (resultSet != null)
						resultSet.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				// Mobile with telecome code
				if (row.getCell(2) != null) {
					if (!row.getCell(2).getStringCellValue().trim().equals("")) {
						objRegistrationTo.setMobileNumber(validateMobileNumber(countryName, row.getCell(2).getStringCellValue().trim()));
					} else {
						objRegistrationTo.setMobileNumber(getDummyMobileNumber(countryName));
					}
				} else {
					objRegistrationTo.setMobileNumber(getDummyMobileNumber(countryName));
				}
				objRegistrationTo.setName(name);
				objRegistrationTo.setBusinessName(bizName);
				objRegistrationTo.setMobileTelecomCode(AppConstants.COUNTRY_CODES.get(countryName));
				objRegistrationTo.setIsPreCreated(true);
				objRegistrationTo.setStatus(AppConstants.IN_QUEUE);
				objRegistrationTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceName());
				//objRegistrationTo.set_id(new ObjectId().toString());
				threadPreparedStatementRegistrations.setString(1, objRegistrationTo.getMobileNumber());
				threadPreparedStatementRegistrations.setString(2, gson.toJson(objRegistrationTo));
				/*DBObject dbObject = (DBObject) JSON.parse(gson.toJson(objRegistrationTo));
				dumpingCollection.insert(dbObject);*/
				MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
				//objMasterCollectionTo.set_id(new ObjectId().toString());
				objMasterCollectionTo.setBizId(null);
				objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
				objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
				objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
				objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
				objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
				objMasterCollectionTo.setName(objRegistrationTo.getName());
				objMasterCollectionTo.setProfileId(null);
				objMasterCollectionTo.setStatus(AppConstants.IN_QUEUE);
				objMasterCollectionTo.setPrfCreatedDate(null);
				threadPreparedStatementMaster.setString(1, objMasterCollectionTo.getMobileNum());
				threadPreparedStatementMaster.setString(2, gson.toJson(objMasterCollectionTo));
				threadPreparedStatementRegistrations.executeUpdate();
				threadPreparedStatementMaster.executeUpdate();
				threadConnection.commit();
				/*masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));*/
				processedMobileNumbers.add(objRegistrationTo.getMobileNumber());
			}
		} else {
			row.createCell(10).setCellValue(errorMessage);
		}
		LOGGER.error(threadName + "::" + rowNumber + " row is processed.");
		if (!errorMessage.isEmpty())
			LOGGER.error(threadName + "::Error message is :: " + errorMessage + ":: for row::" + rowNumber);
	}

	private static ArrayList<String> getBizTypeId(String bizTypeName) {
		ArrayList<String> bizTypeInfo = new ArrayList<String>();
		bizTypeName = bizTypeName.toLowerCase();
		switch (bizTypeName) {
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRITRADER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRITRADER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_AGRITRADER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIBROKER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIBROKER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_BROKER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_FARMER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_FARMER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_FARMER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_TRANSPORTER:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_TRANSPORTER);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_TRANSPORTER);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRIINPUT:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRIINPUT);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_INPUTS);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_AGRISOCIETY:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_AGRISOCIETY);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_SOCIETY);
			return bizTypeInfo;
		case AppConstants.BUSINESS_TYPE_SHEETNAME_WAREHOUSE:
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_ID_WAREHOUSE);
			bizTypeInfo.add(AppConstants.BUSINESS_TYPE_DBNAME_WAREHOUSE);
			return bizTypeInfo;
		default:
			return null;
		}
	}

	private static synchronized String getDummyMobileNumber(String countryName) {
		if (currentDummyMobileNumber == null) {
			try (Statement stmt = hsqlConnection.createStatement(); ResultSet resultSet = stmt.executeQuery("SELECT CURRENTNUMBER FROM DUMMY_NUMBERS;")) {
				while (resultSet.next()) {
					currentDummyMobileNumber = (long) resultSet.getInt("CURRENTNUMBER");
				}
			} catch (Exception e) {
				LOGGER.error("Error in fetching dummy mobile number");
				e.printStackTrace();
			}
		}
		String dummyNumber;
		synchronized (processedMobileNumbers) {
			do {
				currentDummyMobileNumber++;
				dummyNumber = currentDummyMobileNumber.toString();
				while (dummyNumber.length() != 10) {
					dummyNumber = "0" + dummyNumber;
				}
				String mobileTelecomCode = AppConstants.COUNTRY_CODES.get(countryName);
				dummyNumber = mobileTelecomCode + dummyNumber;
			} while (processedMobileNumbers.contains(dummyNumber));
			updateDummyMobileNumber();
			processedMobileNumbers.add(dummyNumber);
		}
		return dummyNumber;
	}

	private static void updateDummyMobileNumber() {
		try (PreparedStatement prepStmt = hsqlConnection.prepareStatement("UPDATE DUMMY_NUMBERS SET CURRENTNUMBER = " + currentDummyMobileNumber + ";")) {
			prepStmt.executeUpdate();
		} catch (Exception e) {
			LOGGER.error("Error in updating dummy number.");
			e.printStackTrace();
		}
	}

	private static String validateMobileNumber(String countryName, String mobileNumber) {
		String[] validMobileNumberLengths = AppConstants.MOBILE_TELECOM_CODES_LENGTH_MAP.get(AppConstants.COUNTRY_CODES.get(countryName)).split(",");
		boolean isValidNumber = false;
		for (String length : validMobileNumberLengths) {
			if (mobileNumber.length() == Integer.valueOf(length))
				isValidNumber = true;
		}
		String mobileTelecomCode = AppConstants.COUNTRY_CODES.get(countryName);
		String mobileNumberWithTelecomeCode = mobileTelecomCode + mobileNumber;
		if (isValidNumber && !processedMobileNumbers.contains(mobileNumberWithTelecomeCode))
			return mobileNumberWithTelecomeCode;
		else
			return getDummyMobileNumber(countryName);
	}

	private static PlaceTo getlocation(String countryId, String subStrings) {
		String comparator = "";
		String spellcheckWord = subStrings;
		PlaceTo objPlaceTo = null;
		try {
			comparator = subStrings;
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
			spellcheckWord = java.net.URLEncoder.encode(spellcheckWord, "UTF-8");
			subStrings = subStrings.replaceAll("\\+", "%20AND%20");
			Response clientResponse = null;
			String URL = PlatformConstants.COMMONDATA_SEARCH
					+ "(filter:location%20AND%20countryId:"
					+ countryId
					+ ")%20AND%20"
					+ subStrings
					+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs%20locationLong%20stateName&start=0&rows=5000&spellcheck.q="
					+ spellcheckWord.replaceAll(" ", "%20OR%20");
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			if (responsedata.get("numFound").toString().equals("0")) {
				JSONObject temp = objJson.getJSONObject("spellcheck");
				JSONObject misSpellArray = null;
				JSONArray queryAndSuggestion = null;
				JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
				for (int i = 0; i < suggestionsArray.length(); i++) {
					if (suggestionsArray.get(i).toString().equalsIgnoreCase(comparator)) {
						misSpellArray = new JSONObject(suggestionsArray.get(i + 1).toString());
						queryAndSuggestion = misSpellArray.getJSONArray("suggestion");
					}
				}
				if (queryAndSuggestion != null) {
					subStrings = queryAndSuggestion.get(0).toString();
				} else {
					return objPlaceTo;
				}
				subStrings = subStrings.replaceAll(" ", "%20AND%20");
				URL = PlatformConstants.COMMONDATA_SEARCH
						+ "(filter:location%20AND%20countryId:"
						+ countryId
						+ ")%20AND%20"
						+ subStrings
						+ "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%20locationAlsoKnownAs%20locationLong%20stateName&start=0&rows=5000";
				webTarget = client.target(URL);
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.get();
				output = clientResponse.readEntity(String.class);
				objJson = new JSONObject(output);
				try {
					if (objJson.has("response")) {
						responsedata = objJson.getJSONObject("response");
					} else {
						return objPlaceTo;
					}
				} catch (Exception e) {
					LOGGER.error("Unable to find response");
				}

			}
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			for (int i = 0; i < array.length();) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				return gson.fromJson(obj1.get("_json_").toString(), PlaceTo.class);
			}
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objPlaceTo;
	}

	/*
	 * private static PlaceTo locationSearch(String countryId, String
	 * locationName) { if (locationsMap.containsKey(locationName)) { return
	 * locationsMap.get(locationName); } try { locationName =
	 * locationName.replaceAll("-", " "); locationName =
	 * java.net.URLEncoder.encode(locationName, "UTF-8"); locationName =
	 * locationName.replaceAll("\\+", "%20AND%20"); Response clientResponse =
	 * null; String URL = PlatformConstants.COMMONDATA_SEARCH +
	 * "(filter:location%20AND%20countryId:" + countryId + ")%20AND%20" +
	 * locationName +
	 * "*&wt=json&spellcheck.dictionary=location&spellcheck=true&spellcheck.build=true&qf=placeName%5E0.8%20stateName%5E0.2%20regionName%5E0.2&start=0&rows=1";
	 * WebTarget webTarget = client.target(URL); Invocation.Builder
	 * invocationBuilder = webTarget.request(); clientResponse =
	 * invocationBuilder.get(); String output =
	 * clientResponse.readEntity(String.class); JSONObject objJson = new
	 * JSONObject(output); JSONObject responsedata =
	 * objJson.getJSONObject("response"); String result =
	 * responsedata.toString(); JSONObject obj = new JSONObject(result); Object
	 * test = obj.get("docs"); JSONArray array = new JSONArray(test.toString());
	 * JSONObject obj1 = new JSONObject(array.get(0).toString()); return
	 * gson.fromJson(obj1.get("_json_").toString(), PlaceTo.class); } catch
	 * (Exception e) { //LOGGER.error("Location not found");
	 * //e.printStackTrace(); return null; } }
	 */

	public static Set<ProductTo> productsSearch(String subStrings, String countryId) {
		Set<ProductTo> productsset = new HashSet<ProductTo>();
		String productIds = "";
		try {
			subStrings = subStrings.replaceAll("-", " ");
			subStrings = subStrings.replaceAll(",", " ");
			// String stateId = locationId.substring(0, 7);
			countryId = countryId.replace(",", " ");
			String URL = "";
			try {
				subStrings = java.net.URLEncoder.encode(subStrings, "UTF-8");
				subStrings = subStrings.replaceAll("\\+", "%20");
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			int rows = 5000;
			Response clientResponse = null;
			URL = PlatformConstants.COMMONDATA_SEARCH + "(countryId:(" + countryId + ")%20AND%20(" + subStrings + "*))&wt=json&spellcheck.dictionary=test&spellcheck=true&"
					+ "qf=%20alsoKnownAs%5E4" + "%20productName%5E5" + "%20localeNames.displayName%5E3" + "%20company1%20crops%5E2%20ingredients1&start=0&rows=" + rows
					+ "&fl=_json_";
			WebTarget webTarget = client.target(URL);
			Invocation.Builder invocationBuilder = webTarget.request();
			clientResponse = invocationBuilder.get();
			String output = clientResponse.readEntity(String.class);
			JSONObject objJson = new JSONObject(output);
			JSONObject responsedata = objJson.getJSONObject("response");
			String result = responsedata.toString();
			JSONObject obj = new JSONObject(result);
			Object test = obj.get("docs");
			JSONArray array = new JSONArray(test.toString());
			String suggestedQuery = null;
			if (obj.get("numFound").toString().equals("0")) {
				JSONObject temp = objJson.getJSONObject("spellcheck");
				JSONArray suggestionsArray = new JSONArray(temp.get("suggestions").toString());
				if (suggestionsArray.length() == 0)
					return productsset;
				if (suggestionsArray.get(3).getClass().equals(JSONArray.class)) {
					JSONArray misSpellArray = new JSONArray(suggestionsArray.get(3).toString());
					JSONArray queryAndSuggestion = new JSONArray(misSpellArray.get(5).toString());
					suggestedQuery = subStrings = queryAndSuggestion.get(1).toString();
				} else {
					JSONObject object = new JSONObject(suggestionsArray.get(3).toString());
					JSONArray array1 = new JSONArray(object.getJSONArray("suggestion").toString());
					suggestedQuery = subStrings = array1.get(0).toString();
				}

				// System.out.println("Did you mean..."+suggestedQuery);
				subStrings = subStrings.replaceAll("-", " ");
				subStrings = subStrings.replaceAll(" ", "%20AND%20");
				URL = PlatformConstants.COMMONDATA_SEARCH
						+ "(countryId:("
						+ countryId
						+ ")%20AND%20*"
						+ subStrings
						+ "*)&wt=json&spellcheck.dictionary=test&spellcheck=true&spellcheck.build=true&qf=productName%20alsoKnownAs%20localeNames.displayName%20company%20crops%20ingredients&start=0&rows=1500&fl=_json_";
				webTarget = client.target(URL);
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.get();
				output = clientResponse.readEntity(String.class);
				objJson = new JSONObject(output);
				responsedata = objJson.getJSONObject("response");
				result = responsedata.toString();
				obj = new JSONObject(result);
				test = obj.get("docs");
				array = new JSONArray(test.toString());
			}
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj1 = new JSONObject(array.get(i).toString());
				SearchProductTo objSearchProductTo = gson.fromJson(obj1.get("_json_").toString(), SearchProductTo.class);
				if (objSearchProductTo.getVarietyId().subSequence(9, 12).equals("000")) {
					if (productsMap.containsKey(objSearchProductTo.getProductId())) {
						if (productsset.size() <= 10)
							productsset.add(productsMap.get(objSearchProductTo.getProductId()));
						else
							return productsset;
					} else {
						/*
						 * ProductTo productTo =
						 * makeProductsTo(objSearchProductTo, stateId,
						 * "SIGNUP"); productsset.add(productTo);
						 * productsMap.put(objSearchProductTo.getProductId(),
						 * productTo);
						 */
						productIds = productIds.concat(objSearchProductTo.getProductId() + ";");
					}
				}
			}
			if (!productIds.isEmpty()) {
				List<ProductTo> productToList = getProductDetails(productIds.substring(0, productIds.length() - 1), countryId);
				if (productToList != null)
					productsset.addAll(productToList);
			}
		} catch (Exception e) {
			// LOGGER.error("Unable to find products.");
			e.printStackTrace();
		}
		return productsset;
	}

	public static List<ProductTo> getProductDetails(String productIds, String countryId) throws SQLException {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		List<ProductTo> productToLst = new ArrayList<ProductTo>();
		List<String> productDupCheck = new ArrayList<String>();
		String[] listofIds = productIds.split(";");
		try {
			String query = createproductInQuery(null, productIds);
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, countryId);
			for (int i = 1; i <= listofIds.length; i++) {
				preparedStatement.setString(i + 1, listofIds[i - 1]);
			}
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				if (!productDupCheck.contains(resultSet.getString("vsspsc_code"))) {
					ProductTo objProductTo = new ProductTo();
					objProductTo.setProductId(resultSet.getString("vsspsc_code"));
					objProductTo.setProductName(resultSet.getString("display_name"));
					objProductTo.setBaseUnitId(resultSet.getInt("unit_id") + "");
					objProductTo.setBaseUnitName(resultSet.getString("unit_name"));
					objProductTo.setCommodityId(resultSet.getString("commodity_id"));
					objProductTo.setCommodityName(resultSet.getString("commodity"));
					objProductTo.setVarietyId(resultSet.getString("variety_id"));
					objProductTo.setVarietyName(resultSet.getString("variety"));
					objProductTo.setScientific_name(resultSet.getString("scientific_name"));
					objProductTo.setUniversal_name(resultSet.getString("universal_name"));
					objProductTo.setNotes(resultSet.getString("notes"));
					objProductTo.setWeblinks(resultSet.getString("weblinks"));
					objProductTo.setAlsoKnownAs(resultSet.getString("also_known_as"));
					objProductTo.setImageUrl(resultSet.getString("image"));
					objProductTo.setQualifiers(resultSet.getString("qualifiers"));
					objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
					objProductTo.setGroupId(resultSet.getString("group_id"));
					objProductTo.setGroupName(resultSet.getString("group_name"));
					objProductTo.setCrops(resultSet.getString("crops"));
					objProductTo.setCompany(resultSet.getString("company_id"));
					objProductTo.setIntegredients(resultSet.getString("ingredients"));
					UnitDeatilsTo details = new UnitDeatilsTo();
					details.setUnitId(resultSet.getInt("unit_id"));
					details.setUnitName(resultSet.getString("unit_name"));
					details.setConversion(resultSet.getDouble("conversion"));
					objProductTo.getListofUnits().add(details);
					objProductTo.setCustomunits(getProductCustomUnits(objProductTo.getProductId(), countryId));
					productToLst.add(objProductTo);
					productDupCheck.add(objProductTo.getProductId());
					synchronized (productsMap) {
						productsMap.put(objProductTo.getProductId(), objProductTo);
					}
				}
			}
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		}
		return productToLst;
	}

	private static String createproductInQuery(String sessionId, String productIds) {
		String query = " SELECT P.ingredients, P.crops, P.company_id,P.vsspsc_code, P.image,P.qualifiers, PL.display_name,P.commodity_id,P.commodity, P.variety_id,P.variety,PG.group_id,P.ALSO_KNOWN_AS,P.SCIENTIFIC_NAME,P.WEBLINKS,P.NOTES,"
				+ " P.UNIVERSAL_NAME,P.HSCODE, PG.group_name, U.unit_id, U.unit_name,U.conversion FROM Products P INNER JOIN Units U on P.unit_id = U.base_unit_id INNER JOIN PRODUCT_GROUPS PG on "
				+ " P.group_id = PG.group_id INNER JOIN PRODUCT_TO_COUNTRY PC on P.vsspsc_code = PC.vsspsc_code INNER JOIN PRODUCTS_LOCALE_NAMES PL on PL.vsspsc_code = PC.vsspsc_code WHERE PL.language_id =1 "
				+ " AND PC.country_id = PL.country_id AND PC.country_id = ? AND P.vsspsc_code IN (";
		StringBuilder queryBuilder = new StringBuilder(query);
		for (int i = 0; i < productIds.split(";").length; i++) {
			queryBuilder.append(" ?");
			if (i != productIds.split(";").length - 1) {
				queryBuilder.append(",");
			}
		}
		queryBuilder.append(") ORDER BY PL.display_name ASC");
		return queryBuilder.toString();
	}

	private static List<UnitDeatilsTo> getProductCustomUnits(String vsspscCode, String countryId) {
		PreparedStatement preparedStatement = null;
		List<UnitDeatilsTo> customUnitsList = new ArrayList<>();
		ResultSet resultSet = null;
		String query = "SELECT custom_unitid, custom_unitname, conversion FROM PRODUCTS_TO_CUSTOMUNITS PC" + " INNER JOIN CUSTOMUNITS C on PC.custom_unitid = C.custom_unitid"
				+ " WHERE PC.vsspsc_code = ? AND C.country_id = ?";
		try {
			preparedStatement = hsqlConnection.prepareStatement(query);
			preparedStatement.setString(1, vsspscCode);
			preparedStatement.setString(2, countryId);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				UnitDeatilsTo objUnitDeatilsTo = new UnitDeatilsTo();
				objUnitDeatilsTo.setUnitId(resultSet.getInt("custom_unitid"));
				objUnitDeatilsTo.setUnitName(resultSet.getString("custom_unitname"));
				objUnitDeatilsTo.setConversion(resultSet.getDouble("conversion"));
				customUnitsList.add(objUnitDeatilsTo);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			LOGGER.error("Problem while fetching Custom units...");
		}
		Collections.sort(customUnitsList, new Comparator<UnitDeatilsTo>() {
			@Override
			public int compare(UnitDeatilsTo o1, UnitDeatilsTo o2) {
				String placeName1 = o1.getUnitName();
				String placeName2 = o2.getUnitName();

				// ascending order
				return placeName1.compareTo(placeName2);
			}

		});
		return customUnitsList;
	}

	/*
	 * private static ProductTo makeProductsTo(SearchProductTo
	 * objSearchProductTo, String stateId, String type) { ProductTo objProductTo
	 * = new ProductTo();
	 * objProductTo.setProductId(objSearchProductTo.getProductId());
	 * objProductTo.setProductName(objSearchProductTo.getProductName());
	 * objProductTo.setBaseUnitId(objSearchProductTo.getBaseUnitId());
	 * objProductTo.setBaseUnitName(objSearchProductTo.getBaseUnitName());
	 * objProductTo.setCommodityId(objSearchProductTo.getCommodityId());
	 * objProductTo.setCommodityName(objSearchProductTo.getCommodityName());
	 * objProductTo.setCrops(objSearchProductTo.getCrops()); if
	 * (objSearchProductTo.getAlsoKnownAs() != null &&
	 * !objSearchProductTo.getAlsoKnownAs().trim().isEmpty()) {
	 * objProductTo.setAlsoKnownAs(objSearchProductTo.getAlsoKnownAs()); } else
	 * { objProductTo.setAlsoKnownAs(""); }
	 * objProductTo.setGroupId(objSearchProductTo.getGroupId());
	 * objProductTo.setGroupName(objSearchProductTo.getGroupName());
	 * objProductTo.setVarietyId(objSearchProductTo.getVarietyId());
	 * objProductTo.setVarietyName(objSearchProductTo.getVarietyName()); if
	 * (objSearchProductTo.getImageUrl() != null) {
	 * objProductTo.setImageUrl(cloudFrontCommonDataImageUrl(objSearchProductTo.
	 * getImageUrl())); } if (!("SIGNUP".equalsIgnoreCase(type))) {
	 * objProductTo.setQualifiers(objSearchProductTo.getQualifiers()); if
	 * (objProductTo.getQualifiers() == null ||
	 * objProductTo.getQualifiers().isEmpty()) {
	 * objProductTo.setQualifiers(getBaseProductQualifiers(objProductTo.
	 * getProductId(), objProductTo.getCommodityId())); }
	 * objProductTo.setQualifiers(qualifiersort(objProductTo.getQualifiers()));
	 * }
	 * 
	 * objProductTo.setProductDescription(objSearchProductTo.
	 * getProductDescription());
	 * objProductTo.setLocaleNames(objSearchProductTo.getStateLocalName().get(
	 * stateId)); if (null ==
	 * objSearchProductTo.getStateLocalName().get(stateId)) { List<LocaleNameTo>
	 * lstOfLocaleNames = getProductLocaleNames(stateId,
	 * objSearchProductTo.getCommodityId());
	 * objProductTo.setLocaleNames(lstOfLocaleNames); }
	 * objProductTo.setProductDisplayName(getProductDisplayName(objProductTo.
	 * getProductName(), objProductTo.getLocaleNames())); return objProductTo; }
	 */

	/*
	 * public static String cloudFrontCommonDataImageUrl(String imageUrl) { if
	 * (imageUrl != null) { String[] images = imageUrl.split(","); return
	 * images[0].replaceFirst(PlatformConstants.COMMONDATA_S3_URL,
	 * PlatformConstants.COMMONDATA_CLOUDFRONT_URL); } return null; }
	 * 
	 * public static String getBaseProductQualifiers(String vsspscCode, String
	 * commodityId) { String qualifiers = null; String query =
	 * "SELECT qualifiers FROM PRODUCTS WHERE vsspsc_code = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?)"
	 * ; try (PreparedStatement preparedStatement =
	 * buildPreparedStatement(query, commodityId, null); ResultSet resultSet =
	 * preparedStatement.executeQuery();) { if (resultSet.next()) { qualifiers =
	 * resultSet.getString("qualifiers"); } } catch (SQLException e) {
	 * e.printStackTrace();
	 * LOGGER.error("Problem while fetching qualifiers..."); } return
	 * qualifiers; }
	 */

	/*
	 * private static PreparedStatement buildPreparedStatement(String query,
	 * String commodityId, String stateId) { PreparedStatement preparedStatement
	 * = null; try { preparedStatement = hsqlConnection.prepareStatement(query);
	 * preparedStatement.setString(1, commodityId + "000"); if (stateId != null)
	 * preparedStatement.setString(2, stateId); } catch (Exception e) {
	 * e.printStackTrace(); if (preparedStatement != null) { try {
	 * preparedStatement.close(); } catch (SQLException e1) {
	 * LOGGER.error("Error while closing prepared statement in catch block.");
	 * e.printStackTrace(); } } } return preparedStatement; }
	 */

	public static String qualifiersort(String qualifier) {
		StringBuilder strbui = new StringBuilder();
		if ((qualifier != null) && !(qualifier.equals(""))) {
			String[] array = qualifier.split(",");
			Arrays.sort(array);
			for (int i = 0; i < array.length; i++) {
				strbui.append(array[i]);
				if ((i + 1) < array.length) {
					strbui.append(",");
				}
			}
			qualifier = strbui.toString();
		}
		return qualifier;
	}

	/*
	 * private static List<LocaleNameTo> getProductLocaleNames(String stateId,
	 * String commodityId) { List<LocaleNameTo> lstOfLocaleNameTo = new
	 * ArrayList<LocaleNameTo>(); String query = ""; if (stateId.substring(0,
	 * 3).contains("335")) { query =
	 * " SELECT PLN.DISPLAY_NAME, LAN.LANGUAGE_NAME, LAN.LANGUAGE_ID FROM LANGUAGES LAN INNER JOIN PRODUCTS_LOCALE_NAMES PLN ON LAN.LANGUAGE_ID = PLN.LANGUAGE_ID INNER JOIN PRODUCTS PDT ON "
	 * +
	 * " PLN.VSSPSC_CODE = PDT.VSSPSC_CODE WHERE PDT.VSSPSC_CODE  = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?) AND LANGUAGE_ID IN(2,(SELECT DEFAULT_LANGUAGE_ID FROM STATES WHERE STATE_ID = ?))"
	 * ; } else { query =
	 * " SELECT PLN.DISPLAY_NAME, LAN.LANGUAGE_NAME, LAN.LANGUAGE_ID FROM LANGUAGES LAN INNER JOIN PRODUCTS_LOCALE_NAMES PLN ON LAN.LANGUAGE_ID = PLN.LANGUAGE_ID INNER JOIN PRODUCTS PDT ON PLN.VSSPSC_CODE = "
	 * +
	 * " PDT.VSSPSC_CODE WHERE PDT.VSSPSC_CODE  = (SELECT vsspsc_code FROM PRODUCTS WHERE variety_id = ?) AND LANGUAGE_ID = (SELECT DEFAULT_LANGUAGE_ID FROM STATES WHERE STATE_ID = ?))"
	 * ; } try (PreparedStatement preparedStatement =
	 * buildPreparedStatement(query, commodityId, stateId); ResultSet
	 * objResultSet = preparedStatement.executeQuery();) { while
	 * (objResultSet.next()) { LocaleNameTo objLocaleNames = new LocaleNameTo();
	 * String localeName = objResultSet.getString("DISPLAY_NAME"); if (null !=
	 * localeName && !localeName.trim().isEmpty()) {
	 * objLocaleNames.setDisplayName(localeName);
	 * objLocaleNames.setLanguageId(objResultSet.getInt("LANGUAGE_ID"));
	 * objLocaleNames.setLanguageName(objResultSet.getString("LANGUAGE_NAME"));
	 * lstOfLocaleNameTo.add(objLocaleNames); } } } catch (Exception e) {
	 * e.printStackTrace(); } return lstOfLocaleNameTo; }
	 * 
	 * private static String getProductDisplayName(String productName,
	 * List<LocaleNameTo> lstOfLocaleNames) { String productDisplayName =
	 * productName; if (2 == lstOfLocaleNames.size()) { for (LocaleNameTo
	 * objLocaleName : lstOfLocaleNames) { // Concatenating other than hindi
	 * language as local name for india users as we are considering hindi as
	 * local lang for all indian users. if (2 != objLocaleName.getLanguageId())
	 * { if
	 * (!productName.trim().equalsIgnoreCase(objLocaleName.getDisplayName().trim
	 * ())) { productDisplayName = productDisplayName + " (" +
	 * objLocaleName.getDisplayName() + ")"; } } } } else if (1 ==
	 * lstOfLocaleNames.size()) { for (LocaleNameTo objLocaleName :
	 * lstOfLocaleNames) { // Excluding local name concatenation if the local
	 * name is in english. if (1 != objLocaleName.getLanguageId()) { if
	 * (!productName.trim().equalsIgnoreCase(objLocaleName.getDisplayName().trim
	 * ())) { productDisplayName = productDisplayName + " (" +
	 * objLocaleName.getDisplayName() + ")"; } } } } return productDisplayName;
	 * }
	 */

}