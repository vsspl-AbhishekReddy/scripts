package transaction.script.excelSheet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class MongoUpdateRecord {
	public static final Logger LOGGER = LoggerFactory.getLogger(MongoUpdateRecord.class);
	static DB db = null;
	static DBCollection testCollection = null;
	static Gson gson = null;
	static {
		try {
			db = MongoDBUtil.getConnection();
			testCollection = db.getCollection("UpdateQueryCollection");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to connect to database");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String mobileNum = "+910000004343";
		String status = "IN_QUEUE";
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("mobileNumber", mobileNum);
		DBCursor dbCursor = testCollection.find(searchQuery);
		while (dbCursor.hasNext()) {
			String dbObject = null;
			dbObject = dbCursor.next().toString();
			UpdateTo objUpdateTo = gson.fromJson(dbObject, UpdateTo.class);
			System.out.println(gson.toJson(objUpdateTo));
			objUpdateTo.setStatus(status);
			System.out.println("@@@@@@@@"+objUpdateTo.get_id().toString());
			String stringData = gson.toJson(objUpdateTo);
			DBObject document = (DBObject) JSON.parse(stringData);
			testCollection.update(new BasicDBObject("_id", objUpdateTo.get_id()), document);
			System.out.println("1111111111111");
		}

	}
}
