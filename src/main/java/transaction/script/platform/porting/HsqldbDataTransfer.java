package transaction.script.platform.porting;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import transaction.script.platform.utils.HsqlDBUtil;

public class HsqldbDataTransfer {

	public static void main(String[] args) {
		Connection dbConnection = HsqlDBUtil.dbConnection;
		try {
			BufferedReader reader = new BufferedReader(new FileReader("/home/abhi/Desktop/test.txt"));
			String query = reader.readLine();
			int lineNumber = 1;
			while (query != null) {
				if (lineNumber % 20000 == 0)
					System.out.println(lineNumber);
				lineNumber++;
				if (query.contains("INSERT INTO LOCATIONS_LOCALE_NAMES VALUES"))
					queryExecutor(dbConnection, query);
				query = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
		List<String> tablesToDeleteAndInsert = Collections.unmodifiableList(Arrays.asList(new String[] { "" }));
		try (PreparedStatement preparedStatement = dbConnection.prepareStatement("SELECT * FROM BIZ_TAX_V2");
				ResultSet resultSet = preparedStatement.executeQuery();
				PreparedStatement preparedStatement2 = dbConnection
						.prepareStatement("INSERT INTO BIZ_TAX_V3 (COUNTRY_ID,LOCATION_ID,COMMODITY_ID,MARKET_FEE,VALUE_ADDED_TAX,CENTRAL_SALES_TAX,NHIL,SALES_TAX,COMMERCIAL_TAX,TAX,SERVICE_TAX,ICMS,ICMS_INTERSTATE,ISS,GST) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");) {
			dbConnection.setAutoCommit(false);
			int i = 1;
			while (resultSet.next()) {
				System.out.println(i++);
				preparedStatement2.setString(1, resultSet.getString("COUNTRY_ID"));
				preparedStatement2.setString(2, resultSet.getString("LOCATION_ID"));
				preparedStatement2.setString(3, resultSet.getString("COMMODITY_ID"));
				preparedStatement2.setString(4, resultSet.getString("MARKET_FEE"));
				preparedStatement2.setString(5, resultSet.getString("VALUE_ADDED_TAX"));
				preparedStatement2.setString(6, resultSet.getString("CENTRAL_SALES_TAX"));
				preparedStatement2.setString(7, resultSet.getString("NHIL"));
				preparedStatement2.setString(8, resultSet.getString("SALES_TAX"));
				preparedStatement2.setString(9, resultSet.getString("COMMERCIAL_TAX"));
				preparedStatement2.setString(10, resultSet.getString("TAX"));
				preparedStatement2.setString(11, resultSet.getString("SERVICE_TAX"));
				preparedStatement2.setString(12, resultSet.getString("ICMS"));
				preparedStatement2.setString(13, resultSet.getString("ICMS_INTERSTATE"));
				preparedStatement2.setString(14, resultSet.getString("ISS"));
				preparedStatement2.setString(15, resultSet.getString("GST"));
				preparedStatement2.execute();
				dbConnection.commit();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				dbConnection.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}

	public static void queryExecutor(Connection conn, String query) {
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null)
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}