package transaction.script.platform.utils;

import transaction.script.constants.AppConstants;

import com.mongodb.DB;
import com.mongodb.Mongo;

public class MongoDBUtil {

	private static DB db = null;

	public synchronized static DB getConnection() throws Exception {
		if (db != null) {
			return db;
		}
		/*List<ServerAddress> seeds = new ArrayList<ServerAddress>();
		seeds.add(new ServerAddress(AppConstants.MONGO_HOST_ONE, 27017));*/
		//seeds.add(new ServerAddress(AppConstants.MONGO_HOST_TWO, 27017));
		Mongo mongo = new Mongo(AppConstants.MONGO_HOST_ONE, AppConstants.MONGO_HOST_PORT);
		db = mongo.getDB("admin");
		//boolean auth = db.authenticate(AppConstants.MONGO_USER, AppConstants.MONGO_PASSWORD.toCharArray());
		if (true) {
			String commonDbName = "commondb";
			db = mongo.getDB(commonDbName);
			return db;
		} else {
			return null;
		}
	}
}