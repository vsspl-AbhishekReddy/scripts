/*******************************************************************************
 * Copyright (c) 2013 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *  
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 *******************************************************************************/
package transaction.script.platform.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;

import transaction.script.models.LocaleNameTo;
import transaction.script.models.UnitDeatilsTo;

public class SearchProductTo {

	@NotNull(message = "{productId.notNull}")
	private String productId;
	@NotNull(message = "{productName.notNull}")
	private String productName;
	@NotNull(message = "{baseUnitId.notNull}")
	private String baseUnitId;
	@NotNull(message = "{baseUnitName.notNUll}")
	private String baseUnitName;
	private String commodityId;
	private String commodityName;
	private String varietyId;
	private String varietyName;
	private String groupId;
	private String groupName;
	private String imageUrl;
	private String productDescription;
	private String qualifiers;
	private String countryId;
	private String countryName;
	private String alsoKnownAs;
	private String id;
	private String filter;
	private List<LocaleNameTo> localeNames = new ArrayList<LocaleNameTo>();
	private List<UnitDeatilsTo> listofUnits = new ArrayList<UnitDeatilsTo>();
	private HashMap<String, List<LocaleNameTo>> stateLocalName = new HashMap<String, List<LocaleNameTo>>();
	private HashMap<String, LocaleNameTo> countryLocalName = new HashMap<String, LocaleNameTo>();
	private String crops;

	public String getCrops() {
		return crops;
	}

	public void setCrops(String crops) {
		this.crops = crops;
	}

	/**
	 * @return the stateLocalName
	 */
	public HashMap<String, List<LocaleNameTo>> getStateLocalName() {
		return stateLocalName;
	}

	/**
	 * @param stateLocalName the stateLocalName to set
	 */
	public void setStateLocalName(HashMap<String, List<LocaleNameTo>> stateLocalName) {
		this.stateLocalName = stateLocalName;
	}

	/**
	 * @return the countryLocalName
	 */
	public HashMap<String, LocaleNameTo> getCountryLocalName() {
		return countryLocalName;
	}

	/**
	 * @param countryLocalName the countryLocalName to set
	 */
	public void setCountryLocalName(HashMap<String, LocaleNameTo> countryLocalName) {
		this.countryLocalName = countryLocalName;
	}

	/**
	 * @return the countryId
	 */
	public String getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}

	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return the alsoKnownAs
	 */
	public String getAlsoKnownAs() {
		return alsoKnownAs;
	}

	/**
	 * @param alsoKnownAs the alsoKnownAs to set
	 */
	public void setAlsoKnownAs(String alsoKnownAs) {
		this.alsoKnownAs = alsoKnownAs;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the filter
	 */
	public String getFilter() {
		return filter;
	}

	/**
	 * @param filter the filter to set
	 */
	public void setFilter(String filter) {
		this.filter = filter;
	}

	/**
	 * @return the localeNames
	 */
	public List<LocaleNameTo> getLocaleNames() {
		return localeNames;
	}

	/**
	 * @param localeNames the localeNames to set
	 */
	public void setLocaleNames(List<LocaleNameTo> localeNames) {
		this.localeNames = localeNames;
	}

	/**
	 * @return the qualifiers
	 */
	public String getQualifiers() {
		return qualifiers;
	}

	/**
	 * @param qualifiers the qualifiers to set
	 */
	public void setQualifiers(String qualifiers) {
		this.qualifiers = qualifiers;
	}

	/**
	 * @return the commodityId
	 */
	public String getCommodityId() {
		return commodityId;
	}

	/**
	 * @param commodityId the commodityId to set
	 */
	public void setCommodityId(String commodityId) {
		this.commodityId = commodityId;
	}

	/**
	 * @return the commodityName
	 */
	public String getCommodityName() {
		return commodityName;
	}

	/**
	 * @param commodityName the commodityName to set
	 */
	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	/**
	 * @return the varietyId
	 */
	public String getVarietyId() {
		return varietyId;
	}

	/**
	 * @param varietyId the varietyId to set
	 */
	public void setVarietyId(String varietyId) {
		this.varietyId = varietyId;
	}

	/**
	 * @return the varietyName
	 */
	public String getVarietyName() {
		return varietyName;
	}

	/**
	 * @param varietyName the varietyName to set
	 */
	public void setVarietyName(String varietyName) {
		this.varietyName = varietyName;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}

	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}

	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the baseUnitId
	 */
	public String getBaseUnitId() {
		return baseUnitId;
	}

	/**
	 * @param baseUnitId the baseUnitId to set
	 */
	public void setBaseUnitId(String baseUnitId) {
		this.baseUnitId = baseUnitId;
	}

	/**
	 * @return the baseUnitName
	 */
	public String getBaseUnitName() {
		return baseUnitName;
	}

	/**
	 * @param baseUnitName the baseUnitName to set
	 */
	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}

	/**
	 * @return the listofUnits
	 */
	public List<UnitDeatilsTo> getListofUnits() {
		return listofUnits;
	}

	/**
	 * @param listofUnits the listofUnits to set
	 */
	public void setListofUnits(List<UnitDeatilsTo> listofUnits) {
		this.listofUnits = listofUnits;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchProductTo other = (SearchProductTo) obj;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		return true;
	}
}