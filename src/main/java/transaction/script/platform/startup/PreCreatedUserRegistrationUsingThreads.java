package transaction.script.platform.startup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.ProductTo;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.PostgresDBUtil;

import com.google.gson.Gson;

public class PreCreatedUserRegistrationUsingThreads implements Runnable {
	public static final Logger LOGGER = LoggerFactory.getLogger(PreCreatedUserRegistrationUsingThreads.class);
	static PreCreatedUserRegistrationUsingThreads classObj = new PreCreatedUserRegistrationUsingThreads();
	static BlockingQueue<RegistrationTo> queue = new LinkedBlockingQueue<RegistrationTo>();
	static Client client = ClientBuilder.newClient();
	//static WebTarget webTarget = client.target("https://www.kalgudi.com/rest/v1/profiles/otpusersignup");
	static WebTarget webTarget = client.target("http://52.2.242.72:8080/rest/v1/profiles/otpusersignup");//kalgudi.com with public IP address
	//static WebTarget webTarget = client.target("http://www.devkalgudi.vasudhaika.net/rest/v1/profiles/otpusersignup");
	//static WebTarget webTarget = client.target("http://192.168.1.67/rest/v1/profiles/otpusersignup");
	static Thread thread = null;
	//static DB db = null;
	//static DBCollection inputCollection = null;
	//static DBCollection masterCollection = null;
	static Gson gson = null;
	static int numberOfUsersRegistered = 0, numberOfUserRegisterationsFailed = 0, numberOfUsersToRegisterInEachSet = 5000, numberOfThreads = 4, initialNumberOfActiveThreads = -1,
			numberOfUsersAlreadyRegistered = 0;
	static long startTime = 0;
	static boolean populatingQueue = false, processData = true;
	static volatile LinkedHashMap<String, RegistrationTo> lastHundredRegistrations = new LinkedHashMap<String, RegistrationTo>() {
		protected boolean removeEldestEntry(Map.Entry<String, RegistrationTo> eldest) {
			return size() > 100;
		}
	};
	static Connection connection = PostgresDBUtil.getConnection("users");
	static PreparedStatement preparedStatementForQueue = null;
	static {
		try {
			preparedStatementForQueue = connection.prepareStatement("select data from registations2 where data->>'status' = 'IN_QUEUE' limit " + numberOfUsersToRegisterInEachSet
					+ ";");
			//db = MongoDBUtil.getConnection();
			//inputCollection = db.getCollection("OCT20");
			//masterCollection = db.getCollection("masterCollectionOCT20");
			//inputCollection = db.getCollection("PreCreatedDataInQueue");
			//masterCollection = db.getCollection("masterCollectionFinal4");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/*if (args.length > 0)
			numberOfThreads = Integer.parseInt(args[0]);
		if (args.length > 1)
			numberOfUsersToRegisterInEachSet = Integer.parseInt(args[1]);*/
		/*try {
			LOGGER.info("Sleeping......");
			TimeUnit.HOURS.sleep(8);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}*/
		LOGGER.info("started.....");
		LOGGER.info(numberOfThreads + ",,,,,," + numberOfUsersToRegisterInEachSet + ",,,,,," + initialNumberOfActiveThreads);
		try {
			while (true) {
				populateQueue();
				if (initialNumberOfActiveThreads == -1)
					initialNumberOfActiveThreads = Thread.activeCount();
				if (startTime == 0)
					startTime = System.currentTimeMillis();
				if (queue.size() == 0)
					break;
				spawnThreads();
				while (true) {
					TimeUnit.SECONDS.sleep(60);//Sleeping the main thread so that this while loop doesn't consume much CPU.
					if (queue.size() == 0 && (Thread.activeCount() - initialNumberOfActiveThreads) == 0) {
						TimeUnit.SECONDS.sleep(30);
						LOGGER.info("Finished waiting for queues and threads to become zero");
						break;
					}
				}
				if ((numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) >= (numberOfUsersToRegisterInEachSet * 20) && queue.size() == 0)
					break;
			}
			LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
			LOGGER.info("Total number of users who are already registered with us :: " + numberOfUsersAlreadyRegistered);
			LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
			LOGGER.info("Abhishek signing off....");
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void populateQueue() {
		populatingQueue = true;
		LOGGER.info("\n\n\nStarted populating queues");
		long mongoGetStartTime = System.currentTimeMillis();
		try (ResultSet resultSet = preparedStatementForQueue.executeQuery()) {
			LOGGER.info("Postgres records fetching time is::: " + (System.currentTimeMillis() - mongoGetStartTime) + "ms");
			long queueFillingStartTime = System.currentTimeMillis();
			while (resultSet.next()) {
				RegistrationTo objRegistrationTo = gson.fromJson(resultSet.getString(1), RegistrationTo.class);
				if (objRegistrationTo.getLstOfProducts().size() > 10) {
					List<ProductTo> lstOfProducts = new ArrayList<>();
					for (ProductTo objProduct : objRegistrationTo.getLstOfProducts()) {
						lstOfProducts.add(objProduct);
						if (lstOfProducts.size() == 10)
							break;
					}
					objRegistrationTo.setLstOfProducts(lstOfProducts);
				}
				queue.offer(objRegistrationTo);
			}
			LOGGER.info("Queue is filled in ::" + (System.currentTimeMillis() - queueFillingStartTime) + "ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//DBCursor cursor = inputCollection.find(new BasicDBObject("status", AppConstants.IN_QUEUE)).limit(numberOfUsersToRegisterInEachSet);
		populatingQueue = false;
		LOGGER.info("Current queue size is :: " + queue.size());
	}

	@Override
	public void run() {
		LOGGER.info("Current thread count is::" + Thread.activeCount());
		Connection threadConnection = null;
		try {
			threadConnection = PostgresDBUtil.getConnection("users");
			threadConnection.setAutoCommit(false);
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
		PreparedStatement threadPreparedStatementMaster = null;
		PreparedStatement threadPreparedStatementRegistrations = null;
		try {
			threadPreparedStatementMaster = threadConnection.prepareStatement("UPDATE master5 SET data = CAST(? AS jsonb) WHERE mobile_number = ?;");
			threadPreparedStatementRegistrations = threadConnection.prepareStatement("UPDATE registations2 SET data = CAST(? AS jsonb) WHERE mobile_number = ?;");
		} catch (SQLException e1) {
			LOGGER.info("Unable to create database statement properly. Interupting current thread.");
			e1.printStackTrace();
			do {
				Thread.currentThread().interrupt();
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} while (Thread.currentThread().isInterrupted());
		}
		while (!Thread.currentThread().isInterrupted()) {
			while (populatingQueue) {
				try {
					TimeUnit.SECONDS.sleep(5);
				} catch (InterruptedException e) {
					LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
					e.printStackTrace();
				}
			}
			if (numberOfUsersRegistered > 0 && numberOfUsersRegistered % 100 == 0) {
				LOGGER.info("Superman in work...");
				LOGGER.info(numberOfUsersRegistered + " users registered until now. Time elapsed is :: " + (System.currentTimeMillis() - startTime) + "ms\n\n\n");
			}
			if (queue.size() == 0) {
				while (Thread.currentThread().isInterrupted())
					Thread.currentThread().interrupt();
				if (Thread.activeCount() - initialNumberOfActiveThreads == 1) {
					LOGGER.info("Superman saves the day. Current data set is processed.");
					LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
					LOGGER.info("Total number of users who are already registered with us :: " + numberOfUsersAlreadyRegistered);
					LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
					LOGGER.info("Total time taken to process " + (numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) + " is :: "
							+ (System.currentTimeMillis() - startTime) + "ms");
				}
				break;
			}
			try {
				RegistrationTo objRegistrationTo = queue.poll();
				if (objRegistrationTo != null) {
					lastHundredRegistrations.put(objRegistrationTo.getMobileNumber(), objRegistrationTo);
					hitPreCreatedUserEndpoint(objRegistrationTo, threadConnection, threadPreparedStatementMaster, threadPreparedStatementRegistrations);
				}
			} catch (Exception e) {
				LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
				e.printStackTrace();
			}
		}
		try {
			if (threadPreparedStatementMaster != null)
				threadPreparedStatementMaster.close();
			if (threadPreparedStatementRegistrations != null)
				threadPreparedStatementRegistrations.close();
			if (threadConnection != null)
				threadConnection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static void hitPreCreatedUserEndpoint(RegistrationTo registrationTo, Connection threadConnection, PreparedStatement threadPreparedStatementMaster,
			PreparedStatement threadPreparedStatementRegistrations) {
		Invocation.Builder invocationBuilder = webTarget.request();
		Response clientResponse = null;
		clientResponse = invocationBuilder.post(Entity.entity(gson.toJson(registrationTo), MediaType.APPLICATION_JSON));
		String response = clientResponse.readEntity(String.class);
		JSONObject jsonObject = new JSONObject(response);
		String responseCode = jsonObject.get("code").toString();
		/*List<BasicDBObject> query = new ArrayList<BasicDBObject>();
		query.add(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()));
		query.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.ACTIVE)));*/
		if (responseCode.equals("200")) {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Success::" + jsonObject.get("info").toString());
			registrationTo.setStatus(AppConstants.ACTIVE);
			//String data = gson.toJson(registrationTo);
			//BasicDBObject basicDbObject = (BasicDBObject) JSON.parse(data);
			//WriteResult result = inputCollection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), basicDbObject, false, true);
			numberOfUsersRegistered++;
		} else {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Error::" + jsonObject.get("error").toString());
			if (responseCode.equals("400")) {
				registrationTo.setStatus(AppConstants.FAIL_DUP_MOBILE);
				//updateMasterCollection(registrationTo, AppConstants.FAIL_DUP_MOBILE);
				numberOfUsersAlreadyRegistered++;
			} else if (responseCode.equals("500")) {
				registrationTo.setStatus(AppConstants.FAIL_EXCEPTION);
				//updateMasterCollection(registrationTo, AppConstants.FAIL_EXCEPTION);
				numberOfUserRegisterationsFailed++;
			} else if (responseCode.equals("123")) {
				LOGGER.error(response);
				Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
				LOGGER.error("\n\n\n\n\n\n\n\n\n\n\n\nFound a major error::::");
				for (String key : lastHundredRegistrations.keySet()) {
					LOGGER.error("data for key id ::" + key + " is ::" + new Gson().toJson(lastHundredRegistrations.get(key)));
				}
				System.exit(1);
			}
			//BasicDBObject basicDBObject = (BasicDBObject) JSON.parse(gson.toJson(registrationTo));
		}
		/*inputCollection.update(
				new BasicDBObject("$and", query),
				new BasicDBObject("$set", new BasicDBObject("isPreCreatedRegComplete", true).append("preCreatedRegInfo", registrationTo.getPreCreatedRegInfo()).append("status",
						registrationTo.getStatus())), false, true);*/
		updateTables(registrationTo, threadConnection, threadPreparedStatementMaster, threadPreparedStatementRegistrations);
	}

	private static void spawnThreads() {
		for (int i = 0; i < numberOfThreads; i++) {
			thread = new Thread(classObj, "TID : " + (i + 1));
			thread.start();
		}
	}

	private static void updateTables(RegistrationTo objRegistrationTo, Connection threadConnection, PreparedStatement threadPreparedStatementMaster,
			PreparedStatement threadPreparedStatementRegistrations) {
		MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
		objMasterCollectionTo.setBizId(null);
		objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
		objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
		objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
		objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
		objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
		objMasterCollectionTo.setName(objRegistrationTo.getName());
		objMasterCollectionTo.setProfileId(null);
		objMasterCollectionTo.setStatus(objRegistrationTo.getStatus());
		objMasterCollectionTo.setPrfCreatedDate(getDate());
		try {
			threadPreparedStatementMaster.setString(1, gson.toJson(objMasterCollectionTo));
			threadPreparedStatementMaster.setString(2, objMasterCollectionTo.getMobileNum());
			threadPreparedStatementRegistrations.setString(1, gson.toJson(objRegistrationTo));
			threadPreparedStatementRegistrations.setString(2, objRegistrationTo.getMobileNumber());
			threadPreparedStatementMaster.executeUpdate();
			threadPreparedStatementRegistrations.executeUpdate();
			threadConnection.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*MasterCollectionTo objMasterCollectionTo = getMongoRecordBasedOnMobileNumber(objRegistrationTo.getMobileNumber());
		System.out.println(gson.toJson(objMasterCollectionTo));
		objMasterCollectionTo.setStatus(status);
		objMasterCollectionTo.setPrfCreatedDate(getDate());
		objMasterCollectionTo.set_id(null);
		List<BasicDBObject> query = new ArrayList<BasicDBObject>();
		query.add(new BasicDBObject("mobileNum", objRegistrationTo.getMobileNumber()));
		query.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.ACTIVE)));
		masterCollection.update(new BasicDBObject("$and", query), new BasicDBObject("$set", new BasicDBObject("status", status).append("prfCreatedDate", getDate())), false, true);*/
	}

	public static String getDate() {
		DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		toFormat.setLenient(false);
		return toFormat.format(new Date());
	}
	
	/*private static MasterCollectionTo getMongoRecordBasedOnMobileNumber(String mobileNo) {
		DBCursor cursor = masterCollection.find(new BasicDBObject("mobileNum", mobileNo));
		while (cursor.hasNext()) {
			MasterCollectionTo obj = gson.fromJson(cursor.next().toString(), MasterCollectionTo.class);
			return obj;
		}
		return null;
	}*/
}