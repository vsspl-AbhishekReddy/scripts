package transaction.script.platform.startup;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import transaction.script.platform.utils.PostgresDBUtil;

public class ReregisterUsers {

	public static void main(String[] args) {
		ArrayList<String> mobileNumbers = new ArrayList<String>();
		Connection connection = PostgresDBUtil.getConnection("users");
		try {
			connection.setAutoCommit(false);
			PreparedStatement preparedStatement = connection
					.prepareStatement("select data->>'mobileNum' from master5 where data->>'prfCreatedDate' > '2016-10-31T06:36:35Z' and data->>'prfCreatedDate' < '2016-11-01T06:36:35Z' and data->>'status' = 'ACTIVE' order by data->>'prfCreatedDate'");
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				mobileNumbers.add(resultSet.getString(1));
			}
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
			System.out.println(mobileNumbers.size());
			int i = 0;
			for (String mobileNumber : mobileNumbers) {
				try {
					URL url = new URL("http://52.2.242.72:8080/rest/v1/profiles/resendPacket/" + mobileNumber);
					System.out.println(++i + ":::" + url.toString());
					URLConnection urlConnection = url.openConnection();
					urlConnection.setConnectTimeout(60000);
					urlConnection.connect();
					System.out.println(IOUtils.toString(urlConnection.getInputStream()) + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
}
