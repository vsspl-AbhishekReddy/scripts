package transaction.script.platform.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CsvFileSplitter {

	public static void main(String[] args) {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/abhi/a/test.csv"))) {
			String line = null;
			int lineNumber = 0, fileNumber = 1;
			File file = null;
			FileWriter fileWriter = null;
			BufferedWriter bufferedWriter = null;
			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
				if (lineNumber == 0) {
					file = new File("/home/abhi/b/" + fileNumber + ".csv");
					file.createNewFile();
					fileWriter = new FileWriter(file);
					bufferedWriter = new BufferedWriter(fileWriter);
				}
				bufferedWriter.append(line + "\n");
				lineNumber++;
				if (lineNumber == 10000) {
					System.out.println("File full");
					lineNumber = 0;
					fileNumber++;
					bufferedWriter.close();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
