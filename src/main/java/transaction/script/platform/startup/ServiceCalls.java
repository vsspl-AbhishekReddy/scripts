package transaction.script.platform.startup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.PostTo;
import transaction.script.models.ProfileAdditionalDetails;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.PostgresDBUtil;

import com.google.gson.Gson;

public class ServiceCalls {
	public static final Logger LOGGER = LoggerFactory.getLogger(ServiceCalls.class);
	static Connection conn = PostgresDBUtil.getConnection("users");
	static PreparedStatement preparedStatement = null;
	static Client client = ClientBuilder.newClient();
	// PRODUCTION -- https://www.kalgudi.com/ LOCAL --
	// http://www.devkalgudi.vasudhaika.net CDE -- 192.168.1.X
	static String domain = "http://192.168.1.6:8080";

	public void callUserEssentails(int limit) {
		WebTarget webTarget = client.target(domain + "/rest/v1/profiles/updateprfaddDetails");
		Invocation.Builder invocationBuilder = webTarget.request();
		Response clientResponse = null;
		List<String> lstOfDetails = getDataFromPostgres("user_additional", limit);
		for (String string : lstOfDetails) {
			ProfileAdditionalDetails objUserEssentails = new Gson().fromJson(string, ProfileAdditionalDetails.class);
			clientResponse = invocationBuilder.post(Entity.entity(new Gson().toJson(objUserEssentails), MediaType.APPLICATION_JSON));
			String response = clientResponse.readEntity(String.class);
			System.out.println(response);
			JSONObject objectResponse = new JSONObject(response);
			if (objectResponse.get("code").equals(200)) {
				objUserEssentails.setResponseInfo(objectResponse.get("info").toString());
			} else if (!objectResponse.get("code").equals(200)) {
				objUserEssentails.setResponseInfo(objectResponse.get("error").toString());
			}
			updatePostGresAdditional(objUserEssentails);
		}
	}

	public void callUserRegistration(int limit) {
		WebTarget webTarget = client.target(domain + "/rest/v1/profiles/otpusersignup");
		Invocation.Builder invocationBuilder = webTarget.request();
		Response clientResponse = null;
		List<String> list = getDataFromPostgres("registations1", limit);
		for (String regDetails : list) {
			RegistrationTo objRegistration = new Gson().fromJson(regDetails, RegistrationTo.class);
			clientResponse = invocationBuilder.post(Entity.entity(new Gson().toJson(objRegistration), MediaType.APPLICATION_JSON));
			String response = clientResponse.readEntity(String.class);
			System.out.println(response);
			JSONObject objectResponse = new JSONObject(response);
			if (objectResponse.get("code").equals(200)) {
				objRegistration.setIsPreCreatedRegComplete(true);
				objRegistration.setPreCreatedRegInfo("Success::" + objectResponse.get("info").toString());
				objRegistration.setStatus(AppConstants.ACTIVE);
			} else {
				objRegistration.setIsPreCreatedRegComplete(true);
				objRegistration.setPreCreatedRegInfo("Error::" + objectResponse.get("error").toString());
				if (objectResponse.get("code").equals(400)) {
					objRegistration.setStatus(AppConstants.FAIL_DUP_MOBILE);
				} else {
					objRegistration.setStatus(AppConstants.FAIL_EXCEPTION);
				}
			}
			updatePostGres(objRegistration);
		}
	}

	public List<String> getDataFromPostgres(String tableName, int limit) {
		ArrayList<String> detailsList = new ArrayList<>();
		System.out.println(limit);
		String sqlQuery = "SELECT data from " + tableName + " WHERE data->>'status' = 'IN_QUEUE' limit ? offset 0";
		try {
			PreparedStatement prepardStatement = conn.prepareStatement(sqlQuery);
			// preparedStatement.setString(1, tableName);
			prepardStatement.setInt(1, limit);
			System.out.println(prepardStatement);
			ResultSet objResult = prepardStatement.executeQuery();
			while (objResult.next()) {
				detailsList.add(objResult.getString("data").toString());
			}
			prepardStatement.close();
			// conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return detailsList;
	}

	public List<PostTo> getPostDataFromPostgres(String tableName, int limit) {
		ArrayList<PostTo> postlist = new ArrayList<>();
		try {
			PreparedStatement prepardStatement = conn.prepareStatement("SELECT id,data from precreated_posts WHERE data->>'status' = 'IN_QUEUE' limit ? offset 0");
			// preparedStatement.setString(1, tableName);
			prepardStatement.setInt(1, limit);
			ResultSet objResult = prepardStatement.executeQuery();
			while (objResult.next()) {
				String data = objResult.getString("data").toString();
				String id = objResult.getString("id");
				PostTo objPostTo = new Gson().fromJson(data, PostTo.class);
				objPostTo.setMobileNum(id);
				postlist.add(objPostTo);
			}
			prepardStatement.close();
			// conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return postlist;
	}

	public void updatePostGres(RegistrationTo objRegistration) {
		try {
			preparedStatement = conn.prepareStatement("UPDATE registations1 SET data = CAST(? AS jsonb) WHERE mobile_number = ?");
			RegistrationTo objRegistrationTo = objRegistration;

			// preparedStatement.setString(1, "registations1");
			String data = new Gson().toJson(objRegistrationTo);
			preparedStatement.setString(1, data);
			preparedStatement.setString(2, objRegistrationTo.getMobileNumber());
			System.out.println(preparedStatement);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (!preparedStatement.isClosed())
					preparedStatement.close();
			} catch (SQLException sqe) {
				sqe.printStackTrace();
			}
		}
	}

	public void updatePostGresAdditional(ProfileAdditionalDetails objProfileAdditionalDetails) {
		try {
			preparedStatement = conn.prepareStatement("UPDATE  user_additional SET data = CAST(? AS jsonb) WHERE id = ? ");
			ProfileAdditionalDetails objUserEssentials = objProfileAdditionalDetails;
			objUserEssentials.setStatus(AppConstants.ACTIVE);
			System.out.println(new Gson().toJson(objUserEssentials));
			String data = new Gson().toJson(objUserEssentials);
			preparedStatement.setString(1, data);
			if (objUserEssentials.getNewMobileNumber() == null || objUserEssentials.getMobileNumber().isEmpty())
				preparedStatement.setString(2, objUserEssentials.getMobileNumber());
			else
				preparedStatement.setString(2, objUserEssentials.getNewMobileNumber());
			preparedStatement.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!preparedStatement.isClosed())
					preparedStatement.close();
			} catch (SQLException sqe) {
				sqe.printStackTrace();
			}
		}
	}

	public void updateProfileAdditionalDetails(int limit) {
		Client clinet = ClientBuilder.newClient();
		WebTarget webTarget = clinet.target("/rest/v1/profiles/updateprfaddDetails");
		Invocation.Builder invocationBuilder = webTarget.request();
		List<String> lstOfDetails = getDataFromPostgres("AddDetails", limit);
		List<ProfileAdditionalDetails> updateDetails = new ArrayList<>();
		Response clientResponse = null;
		for (String str : lstOfDetails) {
			ProfileAdditionalDetails obj = new Gson().fromJson(str, ProfileAdditionalDetails.class);
			clientResponse = invocationBuilder.post(Entity.entity(new Gson().toJson(obj), MediaType.APPLICATION_JSON));
			String response = clientResponse.readEntity(String.class);
			JSONObject jsonObject = new JSONObject(response);
			obj.setStatus("DONE");
			if (jsonObject.get("code").equals("200"))
				obj.setResponseInfo(jsonObject.get("info").toString());
			else if (!jsonObject.get("code").equals("200"))
				obj.setResponseInfo(jsonObject.get("error").toString());
			updateDetails.add(obj);
			if (obj != null && obj.getNewMobileNumber() != null && !obj.getNewMobileNumber().isEmpty())
				updateMobileNoInMasterColl(obj);
		}
		updateAddDetailPostgres(updateDetails);
	}

	public void createPostsForPrecreatedUser(int limit) {
		Client clinet = ClientBuilder.newClient();
		WebTarget webTarget = null;
		Invocation.Builder invocationBuilder = null;
		// List<String> lstOfDetails = getDataFromPostgres("precreated_posts",
		// limit);
		List<PostTo> postDetails = getPostDataFromPostgres("precreated_posts", limit);
		ArrayList<PostTo> updatePostTos = new ArrayList<PostTo>();
		Response clientResponse = null;
		if (postDetails.size() > 0) {
			for (PostTo objPost : postDetails) {
				objPost.setReceivers("ewrwrew");
				System.out.println("post to "+new Gson().toJson(objPost));
				webTarget = clinet.target(domain + "/rest/v1/business/post/postcrawl").queryParam("mobileNumber", objPost.getMobileNum()).queryParam("userName", "aaaa");
				invocationBuilder = webTarget.request();
				clientResponse = invocationBuilder.post(Entity.entity(new Gson().toJson(objPost), MediaType.APPLICATION_JSON));
				String response = clientResponse.readEntity(String.class);
				System.out.println(response);
				JSONObject jsonObject = new JSONObject(response);
				objPost.setStatus("DONE");
				if (jsonObject.get("code").equals(201)){
					objPost.setResponseInfo(jsonObject.get("info").toString());
				objPost.setStatus(AppConstants.ACTIVE);
				}
				else if (jsonObject.get("code").equals(520)){
					objPost.setResponseInfo(jsonObject.get("info").toString());
					objPost.setStatus(AppConstants.USER_ACTIVE);
				}
				else if (!jsonObject.get("code").equals(201) && !jsonObject.get("code").equals(520)){
					objPost.setResponseInfo(jsonObject.get("error").toString());
				objPost.setStatus(AppConstants.ACTIVE);
				}
				updatePostTos.add(objPost);
			}
		} else {
			LOGGER.info("unable to get the post objects from database ");
		}
		updatePostDetails(updatePostTos);
	}

	public void updatePostDetails(List<PostTo> userPostDetails) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn.prepareStatement("UPDATE precreated_posts SET data = CAST(? AS jsonb) WHERE data->>'identifierTimestamp' = ?");
			for (PostTo objPosts : userPostDetails) {
				
				String data = new Gson().toJson(objPosts);
				preparedStatement.setString(1, data);
				preparedStatement.setString(2, objPosts.getIdentifierTimestamp());
				preparedStatement.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (preparedStatement != null)
					preparedStatement.close();
			} catch (Exception e) {
			}
		}
	}

	private void updateMobileNoInMasterColl(ProfileAdditionalDetails obj) {
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = conn.prepareStatement("UPDATE masterCollection SET id = ? WHERE id = ? ");
			preparedStatement.setString(1, obj.getMobileNumber());
			preparedStatement.setString(2, obj.getNewMobileNumber());
			preparedStatement.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			LOGGER.error("Exception occured while updating usermobile number in master collection " + e.toString());
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateAddDetailPostgres(List<ProfileAdditionalDetails> details) {
		try {
			preparedStatement = conn.prepareStatement("UPDATE AddDetails SET data->>'status' = 'DONE', data->>'responseInfo' = ? WHERE id = ?");
			for (ProfileAdditionalDetails obj : details) {
				preparedStatement.setString(1, obj.getResponseInfo());
				preparedStatement.setString(2, obj.getMobileNumber());
				preparedStatement.executeQuery();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			LOGGER.error(" Exception occured while updating status in addDetails table " + e.toString());
		}
	}

	public static void main(String[] args) {
		ServiceCalls objServiceCalls = new ServiceCalls();
		objServiceCalls.createPostsForPrecreatedUser(1);
		//objServiceCalls.callUserRegistration(700);
	//objServiceCalls.callUserEssentails(20);
	}
}
