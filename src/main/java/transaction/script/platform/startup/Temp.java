/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.mongodb.BasicDBObject
 *  com.mongodb.DB
 *  com.mongodb.DBCollection
 *  com.mongodb.DBCursor
 *  com.mongodb.DBObject
 *  com.mongodb.WriteResult
 *  javax.ws.rs.client.Client
 *  javax.ws.rs.client.ClientBuilder
 *  javax.ws.rs.client.Entity
 *  javax.ws.rs.client.Invocation
 *  javax.ws.rs.client.Invocation$Builder
 *  javax.ws.rs.client.WebTarget
 *  javax.ws.rs.core.Response
 *  org.json.JSONObject
 *  org.slf4j.Logger
 *  org.slf4j.LoggerFactory
 *  transaction.script.models.RegistrationTo
 *  transaction.script.platform.utils.MongoDBUtil
 */
package transaction.script.platform.startup;

import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class Temp implements Runnable {
	public static final Logger LOGGER = LoggerFactory.getLogger(PreCreatedUserRegistrationUsingThreads.class);
	static PreCreatedUserRegistrationUsingThreads classObj = new PreCreatedUserRegistrationUsingThreads();
	static BlockingQueue<RegistrationTo> queue = new LinkedBlockingQueue<RegistrationTo>();
	private static Client client = ClientBuilder.newClient();
	static String ipAddress = "";
	static Thread thread = null;
	static int numberOfThreads = 4;
	static int initialNumberOfActiveThreads = 0;
	static boolean populatingQueue = false;
	static boolean processData = true;
	static DB db = null;
	static DBCollection collection = null;
	static DBCollection badUsersCollection = null;
	static Gson gson = null;
	static int numberOfUsersRegistered = 0;
	static int numberOfUserRegisterationsFailed = 0;
	static int numberOfUsersToRegister = 1000;
	static RegistrationTo objRegistrationToForDummyUser = null;
	static long startTime = System.currentTimeMillis();
	static Scanner scanner = null;
	static String mobileNumber = "";
	static String firstName = "";
	static String businessName = "";

	public static void main(String[] args) {
		try {
			populateQueue();
			spawnThreads();
			System.out.println("Abhishek signing off....");
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void populateQueue() {
		populatingQueue = true;
		LOGGER.info("Started populating queues");
		DBCursor cursor = collection.find((DBObject) new BasicDBObject("isPreCreatedRegComplete", (Object) false)).sort((DBObject) new BasicDBObject("_id", (Object) 1))
				.limit(numberOfUsersToRegister);
		while (cursor.hasNext()) {
			String data = cursor.next().toString();
			RegistrationTo objRegistrationTo = gson.fromJson(data, RegistrationTo.class);
			queue.offer(objRegistrationTo);
		}
		populatingQueue = false;
		System.out.println("Current queue size is :: " + queue.size());
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			RegistrationTo registrationTo;
			while (populatingQueue) {
				try {
					TimeUnit.SECONDS.sleep(20);
				} catch (InterruptedException e) {
					LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
					e.printStackTrace();
				}
			}
			if (numberOfUsersRegistered > 0 && numberOfUsersRegistered % 100 == 0) {
				LOGGER.info("\n\n\n\nsuperman in work...");
				LOGGER.info("" + numberOfUsersRegistered + " users registered until now. Time elapsed is :: " + (System.currentTimeMillis() - startTime) + "ms");
			}
			if (queue.size() == 0) {
				LOGGER.info("\n\n\nsuperman saves the day");
				LOGGER.info("All data is processed.");
				LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
				LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
				LOGGER.info("Total time taken to process " + (numberOfUsersRegistered + numberOfUserRegisterationsFailed) + " is :: " + (System.currentTimeMillis() - startTime)
						+ "ms");
			}
			if ((registrationTo = queue.poll()) == null) {
				Thread.currentThread().interrupt();
				break;
			}
			hitPreCreatedUserEndpoint(registrationTo);
		}
	}

	private static void hitPreCreatedUserEndpoint(RegistrationTo registrationTo) {
		WebTarget webTarget = client.target("https://www.kalgudi.com/rest/v1/profiles/otpusersignup");
		Invocation.Builder invocationBuilder = webTarget.request();
		Response clientResponse = invocationBuilder.post(Entity.entity((Object) gson.toJson((Object) registrationTo), (String) "application/json"));
		JSONObject jsonObject = new JSONObject(clientResponse.readEntity(String.class));
		if (jsonObject.get("code").toString().equals("200")) {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Success::" + jsonObject.get("info").toString());
			collection.update((DBObject) new BasicDBObject("mobileNumber", (Object) registrationTo.getMobileNumber()),
					(DBObject) gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class));
			++numberOfUsersRegistered;
			if (objRegistrationToForDummyUser == null) {
				objRegistrationToForDummyUser = registrationTo;
			}
		} else {
			System.out.println("bad things are happening");
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Error::" + jsonObject.get("error").toString());
			collection.update((DBObject) new BasicDBObject("mobileNumber", (Object) registrationTo.getMobileNumber()),
					(DBObject) gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class));
			++numberOfUserRegisterationsFailed;
		}
	}

	private static void spawnThreads() {
		for (int i = 0; i < numberOfThreads; ++i) {
			thread = new Thread((Runnable) classObj, "TID : " + (i + 1));
			thread.start();
		}
	}

	static {
		try {
			db = MongoDBUtil.getConnection();
			collection = db.getCollection("PreCreatedUsersDataColl");
			gson = new Gson();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}