package transaction.script.platform.startup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class LocalMongoDataCorrector {
	public static final Logger LOGGER = LoggerFactory.getLogger(LocalMongoDataCorrector.class);
	static DB db = null;
	static DBCollection productionPreCreatedUsersCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static int numberOfUsersEligibleForDeletion = 0;
	static {
		try {
			db = MongoDBUtil.getConnection();
			productionPreCreatedUsersCollection = db.getCollection("ProductionPrecreatedDataCollTwo");
			masterCollection = db.getCollection("masterCollectionSEP23");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise scripts");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int numberOfInValidUsers = 0;
		int numberOfValidUsersInserted = 0;
		int numberOfRecordsProcessed = 0;
		@SuppressWarnings("unchecked")
		List<String> mobileNumbersList = masterCollection.distinct("mobileNum");
		Set<String> processedMobileNumbers = new HashSet<String>(mobileNumbersList);
		LOGGER.info("\n\n\n");
		LOGGER.info("Started processing new set...");
		List<BasicDBObject> list1 = new ArrayList<BasicDBObject>();
		list1.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.ACTIVE)));
		list1.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.IN_QUEUE)));
		list1.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.DUPLICATE_ALL)));
		list1.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.DUPLICATE_MOBILE)));
		list1.add(new BasicDBObject("status", new BasicDBObject("$ne", AppConstants.DUPLICATE_DATA)));
		list1.add(new BasicDBObject("isPreCreatedRegComplete", false));
		BasicDBObject searchQuery = new BasicDBObject("$and", list1);
		DBCursor data = productionPreCreatedUsersCollection.find(searchQuery);
		while (data.hasNext()) {
			boolean isDataMatch = false;
			boolean isMobileNumberMatch = false;
			numberOfRecordsProcessed++;
			RegistrationTo objRegistrationTo = gson.fromJson(data.next().toString(), RegistrationTo.class);
			String name = objRegistrationTo.getName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ").replace("{", " ").replace("}", " ");
			String bizName = objRegistrationTo.getBusinessName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ").replace("+", " ").replace("{", " ")
					.replace("}", " ");
			List<BasicDBObject> list = new ArrayList<BasicDBObject>();
			list.add(new BasicDBObject("name", Pattern.compile("^" + name + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizName", Pattern.compile("^" + bizName + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizTypeId", objRegistrationTo.getBusinessTypeId()));
			list.add(new BasicDBObject("locationId", objRegistrationTo.getLocationTo().getPlaceId()));
			DBCursor masterCollectionDbCursor = masterCollection.find(new BasicDBObject("$and", list));
			if (masterCollectionDbCursor.hasNext())
				isDataMatch = true;
			if (processedMobileNumbers.contains(objRegistrationTo.getMobileNumber()))
				isMobileNumberMatch = true;
			if (isMobileNumberMatch && isDataMatch) {
				objRegistrationTo.setStatus(AppConstants.DUPLICATE_ALL);
			} else if (isMobileNumberMatch && !isDataMatch) {
				objRegistrationTo.setStatus(AppConstants.DUPLICATE_MOBILE);
			} else if (!isMobileNumberMatch && isDataMatch) {
				objRegistrationTo.setStatus(AppConstants.DUPLICATE_DATA);
			} else {
				objRegistrationTo.setStatus(AppConstants.IN_QUEUE);
			}
			if (!objRegistrationTo.getStatus().equals(AppConstants.IN_QUEUE)) {
				numberOfInValidUsers++;
				if (isDataMatch) {
					MasterCollectionTo objMasterCollectionTo = gson.fromJson(masterCollectionDbCursor.next().toString(), MasterCollectionTo.class);
					objRegistrationTo.setDuplicateOfMobileNumber("DATA DUP :: " + objMasterCollectionTo.getMobileNum());
				}
				if (isMobileNumberMatch) {
					objRegistrationTo.setDuplicateOfMobileNumber("PHONE DUP :: " + objRegistrationTo.getMobileNumber());
				}
			}
			productionPreCreatedUsersCollection.update(new BasicDBObject("mobileNumber", objRegistrationTo.getMobileNumber()),
					new BasicDBObject("$set", new BasicDBObject("status", objRegistrationTo.getStatus()).append("duplicateOfMobileNumber", objRegistrationTo.getDuplicateOfMobileNumber())), false,
					true);
			if (objRegistrationTo.getStatus().equals(AppConstants.IN_QUEUE)) {
				processedMobileNumbers.add(objRegistrationTo.getMobileNumber());
				MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
				//objMasterCollectionTo.set_id(new ObjectId().toString());
				objMasterCollectionTo.setBizId(null);
				objMasterCollectionTo.setBizName(objRegistrationTo.getBusinessName());
				objMasterCollectionTo.setBizTypeId(objRegistrationTo.getBusinessTypeId());
				objMasterCollectionTo.setLocationId(objRegistrationTo.getLocationTo().getPlaceId());
				objMasterCollectionTo.setMobileNum(objRegistrationTo.getMobileNumber());
				objMasterCollectionTo.setCountryCode(objRegistrationTo.getMobileTelecomCode());
				objMasterCollectionTo.setName(objRegistrationTo.getName());
				objMasterCollectionTo.setProfileId(null);
				objMasterCollectionTo.setStatus(AppConstants.IN_QUEUE);
				objMasterCollectionTo.setPrfCreatedDate(formatter(new Date()));
				masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
				numberOfValidUsersInserted++;
			}
			if (numberOfRecordsProcessed % 1000 == 0) {
				LOGGER.info("\n\nNumber of records processed currently :: " + numberOfRecordsProcessed);
				LOGGER.info("Number of valid users currently inserted in master for queuing :: " + numberOfValidUsersInserted);
				LOGGER.info("Number of in-valid users currently rejected as duplicates :: " + numberOfInValidUsers);
			}
		}
		LOGGER.info("\n\nNumber of records processed :: " + numberOfRecordsProcessed);
		LOGGER.info("Number of valid users currently inserted in master for queuing :: " + numberOfValidUsersInserted);
		LOGGER.info("Number of in-valid users currently rejected as duplicates :: " + numberOfInValidUsers);
		LOGGER.info("Abhishek signing off...");
	}

	private static String formatter(Date date) {
		DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		toFormat.setLenient(false);
		return toFormat.format(date);
	}
}