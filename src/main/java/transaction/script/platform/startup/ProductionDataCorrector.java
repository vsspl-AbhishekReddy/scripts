package transaction.script.platform.startup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.UserProfileDataTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class ProductionDataCorrector {
	public static final Logger LOGGER = LoggerFactory.getLogger(ProductionDataCorrector.class);
	static DB db = null;
	static DBCollection productionCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static int numberOfUsersEligibleForDeletion = 0;
	static Set<String> processedMobileNumbers;
	static int numberOfInValidUsers = 0;
	static {
		try {
			db = MongoDBUtil.getConnection();
			productionCollection = db.getCollection("productionCollection");
			masterCollection = db.getCollection("masterCollectionFinal4");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise data");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int numberOfValidUsersInserted = 0;
		int numberOfRecordsProcessed = 0;
		@SuppressWarnings("unchecked")
		List<String> mobileNumbersList = masterCollection.distinct("mobileNum");
		processedMobileNumbers = new HashSet<String>(mobileNumbersList);
		List<BasicDBObject> list2 = new ArrayList<BasicDBObject>();
		list2.add(new BasicDBObject("prfCreatedDate", new BasicDBObject("$gte", "2016-09-14T00:00:00Z")));
		list2.add(new BasicDBObject("prfCreatedDate", new BasicDBObject("$lte", "2016-09-17T00:00:00Z")));
		DBCursor productionData = productionCollection.find(new BasicDBObject("$and", list2));
		//DBCursor productionData = productionCollection.find();
		LOGGER.info("\n\n\n");
		LOGGER.info("Started processing new set...");
		while (productionData.hasNext()) {
			numberOfRecordsProcessed++;
			UserProfileDataTo userProfileData = gson.fromJson(productionData.next().toString(), UserProfileDataTo.class);
			List<BasicDBObject> list = new ArrayList<BasicDBObject>();
			String name = userProfileData.getFirstName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ").replace("\\", " ");
			String bizName = userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessName().replace("(", " ").replace(")", " ").replace("[", " ").replace("]", " ")
					.replace("\\", " ");
			list.add(new BasicDBObject("name", Pattern.compile("^" + name + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizName", Pattern.compile("^" + bizName + "$", Pattern.CASE_INSENSITIVE)));
			list.add(new BasicDBObject("bizTypeId", userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessTypeId()));
			list.add(new BasicDBObject("locationId", userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getLocationTo().getPlaceId()));
			DBCursor masterCollectionDbCursor = masterCollection.find(new BasicDBObject("$and", list));
			if (masterCollectionDbCursor.hasNext()) {
				while (masterCollectionDbCursor.hasNext()) {
					MasterCollectionTo objMasterCollectionTo = gson.fromJson(masterCollectionDbCursor.next().toString(), MasterCollectionTo.class);
					if (!processedMobileNumbers.contains(userProfileData.getMobileNo())) {
						if (!objMasterCollectionTo.getStatus().equals(AppConstants.DUPLICATE)) {
							objMasterCollectionTo.setStatus(AppConstants.DUPLICATE);
							/*masterCollection.update(new BasicDBObject("_id", objMasterCollectionTo.get_id()),
									gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class), true, false);*/
							numberOfInValidUsers++;
							numberOfValidUsersInserted--;
							insertCurrentUser(userProfileData, name, bizName);
						}
					}
				}
			} else {
				if (!processedMobileNumbers.contains(userProfileData.getMobileNo())) {
					processedMobileNumbers.add(userProfileData.getMobileNo());
					MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
					//objMasterCollectionTo.set_id(new ObjectId().toString());
					objMasterCollectionTo.setBizId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessKey());
					objMasterCollectionTo.setBizName(bizName);
					objMasterCollectionTo.setBizTypeId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessTypeId());
					objMasterCollectionTo.setLocationId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getLocationTo().getPlaceId());
					objMasterCollectionTo.setMobileNum(userProfileData.getMobileNo());
					objMasterCollectionTo.setCountryCode(userProfileData.getMobileCode());
					objMasterCollectionTo.setName(name);
					objMasterCollectionTo.setProfileId(userProfileData.getProfileKey());
					objMasterCollectionTo.setStatus(AppConstants.ACTIVE);
					objMasterCollectionTo.setPrfCreatedDate(userProfileData.getPrfCreatedDate());
					masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
					numberOfValidUsersInserted++;
				}
			}
			if (numberOfRecordsProcessed % 1000 == 0) {
				LOGGER.info("\n\nNumber of records processed currently :: " + numberOfRecordsProcessed);
				LOGGER.info("Number of valid users currently :: " + numberOfValidUsersInserted);
				LOGGER.info("Number of in-valid users currently :: " + numberOfInValidUsers);
			}
		}
		LOGGER.info("\n\nNumber of records processed :: " + numberOfRecordsProcessed);
		LOGGER.info("Number of valid users :: " + numberOfValidUsersInserted);
		LOGGER.info("Number of in-valid users :: " + numberOfInValidUsers);
		LOGGER.info("Abhishek signing off...");
	}

	private static void insertCurrentUser(UserProfileDataTo userProfileData, String name, String bizName) {
		processedMobileNumbers.add(userProfileData.getMobileNo());
		MasterCollectionTo objMasterCollectionTo = new MasterCollectionTo();
		//objMasterCollectionTo.set_id(new ObjectId().toString());
		objMasterCollectionTo.setBizId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessKey());
		objMasterCollectionTo.setBizName(bizName);
		objMasterCollectionTo.setBizTypeId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessTypeId());
		objMasterCollectionTo.setLocationId(userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getLocationTo().getPlaceId());
		objMasterCollectionTo.setMobileNum(userProfileData.getMobileNo());
		objMasterCollectionTo.setCountryCode(userProfileData.getMobileCode());
		objMasterCollectionTo.setName(name);
		objMasterCollectionTo.setProfileId(userProfileData.getProfileKey());
		objMasterCollectionTo.setStatus(AppConstants.DUPLICATE);
		objMasterCollectionTo.setPrfCreatedDate(userProfileData.getPrfCreatedDate());
		masterCollection.insert(gson.fromJson(gson.toJson(objMasterCollectionTo), BasicDBObject.class));
		numberOfInValidUsers++;
	}
}